﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Retoxi
{
    public partial class MainForm : Form
    {
        private List<Ital> italok = new List<Ital>();

        public MainForm()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ReadFile())
            {
                itallapToolStripMenuItem.Enabled = true;
                saveToolStripMenuItem.Enabled = true;

                ShowItallap();
            }
        }

        private bool ReadFile()
        {
            bool retVal = false;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamReader reader = null;
                try
                {
                    reader = new StreamReader(openFileDialog.FileName);
                    string[] dataItems;
                    while (!reader.EndOfStream)
                    {
                        dataItems = reader.ReadLine().Split(';');
                        italok.Add(
                            new Ital(
                                dataItems[0],
                                int.Parse(dataItems[1])
                            )
                        );
                    }
                    retVal = true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        String.Format("Hiba történt a fájl olvasása közben! {0}", ex.Message),
                        "Figyeleme!"
                    );
                }
                finally
                {
                    if (reader != null) reader.Close();
                }
            }

            return retVal;
        }

        private void itallapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowItallap();
        }

        private void ShowItallap()
        {
            ItallapForm itallapForm = new ItallapForm(italok);
            itallapForm.ShowDialog();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(
                "Készült a Programozás 3 tantárgy 09. gyakorlatán (2020)", 
                "Névjegy",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HelpForm helpForm = new HelpForm();

            //Result of dialog window received and handled
            if (helpForm.ShowDialog() == DialogResult.OK)
            {
                MessageBox.Show("Ügyes!");
            }
            else
            {
                MessageBox.Show("Figyeljen jobban a bezárásra!");
            }

            //Get and use data from HelpForm instance
            switch (helpForm.DidHelp) {
                case HelpForm.HelpUsefulness.Yes:
                    MessageBox.Show("Örülök, hogy a súgó segített! :)");
                    break;
                case HelpForm.HelpUsefulness.No:
                    MessageBox.Show("Sajnálom, hogy a súgó nem segített! :(");
                    break;
                case HelpForm.HelpUsefulness.NotSelected:
                    MessageBox.Show("Nem tudom, hogy a súgó segített-e! :|");
                    break;
            }
        }

        private void galeryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GalleryForm galleryForm = new GalleryForm();
            galleryForm.ShowDialog();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                StreamWriter writer = new StreamWriter(saveFileDialog.FileName);
                writer.WriteLine("Eladott italadagok:");
                foreach(Ital ital in italok)
                {
                    writer.WriteLine(ital.ToString());
                }
                writer.Close();
            }
        }
    }
}
