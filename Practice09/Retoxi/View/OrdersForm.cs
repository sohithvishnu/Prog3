﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Retoxi
{
    public partial class OrdersForm : Form
    {
        private List<Ital> italok;

        public OrdersForm(List<Ital> italok)
        {
            InitializeComponent();

            this.italok = italok;
        }

        private void OrdersForm_Load(object sender, EventArgs e)
        {
            Orders.Text = "Rendelés összesítő:";
            Orders.Text += Environment.NewLine;

            int total = 0;
            foreach (Ital ital in italok)
            {
                if(ital.Fizetendo > 0)
                {
                    Orders.Text += ital.ToString();
                    Orders.Text += Environment.NewLine;
                }
                total += ital.Fizetendo;
            }

            Orders.Text += "Összesen: " + total.ToString();
        }
    }
}
