﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Retoxi
{
    public partial class GalleryForm : Form
    {
        private List<Image> images = new List<Image>();
        private int currentIndex = 0;

        public GalleryForm()
        {
            InitializeComponent();
        }

        private void GalleryForm_Load(object sender, EventArgs e)
        {
            LoadImages();
            ShowCurrentImage();
        }

        private void LoadImages()
        {
            images.Clear();
            for (int i = 1; i <= 10; i++)
            {
                images.Add(Image.FromFile("..\\..\\Images\\kep" + i.ToString() + ".jpg"));
            }
        }

        private void ShowCurrentImage()
        {
            pictureBox.Image = images[currentIndex];
        }

        private void Previous_Click(object sender, EventArgs e)
        {
            if (--currentIndex < 0) currentIndex = images.Count - 1;
            ShowCurrentImage();
        }

        private void Next_Click(object sender, EventArgs e)
        {
            if (++currentIndex >= images.Count) currentIndex = 0;
            ShowCurrentImage();
        }
    }
}
