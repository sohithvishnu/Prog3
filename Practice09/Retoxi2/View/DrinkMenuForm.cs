﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Retoxi
{
    public partial class DrinkMenuForm : Form
    {
        private DrinkStorage storage;
        private List<DrinkMenuControl> controls = new List<DrinkMenuControl>();

        public DrinkMenuForm(DrinkStorage storage)
        {
            InitializeComponent();
            this.storage = storage;
        }

        private const int bal = 10, fent = 10;
        private const int chkXMeret = 250, chkYMeret = 21;
        private const int chkYTav = 40;
        private const int txtXMeret = 30, txtYMeret = 17;
        private const int lblXMeret = 46;

        private const int xKoz = 5;

        private const int maxAdag = 999;

        private void ItallapForm_Load(object sender, EventArgs e)
        {
            CreateDynamicFormContent();
        }

        private void CreateDynamicFormContent()
        {
            for(int i=0; i<storage.Count; i++)
            {
                CreateControlsOfItem(storage[i], i);
            }
        }

        private void CreateControlsOfItem(Drink item, int index)
        {
            CheckBox nev;
            TextBox db;
            Label adag;

            nev = new CheckBox();
            nev.Text = DrinkPrinter.PrintToPriceList(item);
            nev.Location = new Point(bal, fent + index * chkYTav);
            nev.Size = new Size(chkXMeret, chkYMeret);

            db = new TextBox();
            db.Location = new Point(
                bal + nev.Size.Width + xKoz,
                fent + index * chkYTav
            );
            db.MaxLength = 3;
            db.Size = new Size(txtXMeret, txtYMeret);

            adag = new Label();
            adag.Text = "adag";
            adag.Location = new Point(
                db.Location.X + db.Size.Width + xKoz,
                fent + index * chkYTav
            );
            adag.Size = new Size(lblXMeret, txtYMeret);

            ContentContainer.Controls.Add(nev);
            ContentContainer.Controls.Add(db);
            ContentContainer.Controls.Add(adag);

            controls.Add(new DrinkMenuControl(item, nev, db));
        }

        private void Order_Click(object sender, EventArgs e)
        {
            bool hasError = false;
            foreach(var control in controls)
            {
                if (control.Checked)
                {
                    int db = 0;
                    bool isNumber = int.TryParse(control.Pieces, out db);

                    if (!isNumber || db < 1 || db > maxAdag)
                    {
                        control.HasError = true;
                        hasError = true;
                    }
                    else
                    {
                        control.Model.UnpayedOrder += db;
                        control.Reset();
                    }
                }
                else
                {
                    control.Reset();
                }
            }

            if (hasError)
            {
                MessageBox.Show(
                    "A pirossal jelzett adatok hibásak!",
                    "Figyelem!"
                );
            }
        }

        private void billToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OrdersForm ordersForm = new OrdersForm(storage);
            ordersForm.ShowDialog();
        }

        private void payToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach(Drink ital in storage)
            {
                ital.PayedOrder += ital.UnpayedOrder;
                ital.UnpayedOrder = 0;
            }

            MessageBox.Show("A számla fizetve!");
        }
    }
}
