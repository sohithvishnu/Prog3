﻿using System;
using System.Windows.Forms;

namespace Retoxi
{
    public partial class MainForm : Form
    {
        private DrinkStorage storage = new DrinkStorage();

        public MainForm()
        {
            InitializeComponent();
        }

        #region Menu event handlers

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReadFile();

            EnableUI();
        }

        private void itallapToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ShowItallap();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Készült a Programozás 3 tantárgy 09. gyakorlatán (2020)", "Névjegy");
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HelpForm helpForm = new HelpForm();

            helpForm.ShowDialog();
        }

        private void galeryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GalleryForm galleryForm = new GalleryForm();
            galleryForm.ShowDialog();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                DrinkPrinter.PrintListToFile(
                    storage, 
                    saveFileDialog1.FileName, 
                    DrinkPrinter.PrintToInventory
                );
            }
        }

        #endregion

        private void ReadFile()
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    storage.LoadFromFile(openFileDialog1.FileName, new DrinkParser());
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        String.Format("Hiba történt a fájl olvasása közben! {0}", ex.Message),
                        "Figyeleme!"
                    );
                }
            }
        }

        private void EnableUI()
        {
            bool success = storage.Count > 0;
            itallapToolStripMenuItem.Enabled = success;
            saveToolStripMenuItem.Enabled = success;

            if (success)
            {
                ShowItallap();
            }
        }

        private void ShowItallap()
        {
            DrinkMenuForm itallapForm = new DrinkMenuForm(storage);
            itallapForm.ShowDialog();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if(MessageBox.Show("Biztos be szeretné zárni az ablakot?", "Figyelem!", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}
