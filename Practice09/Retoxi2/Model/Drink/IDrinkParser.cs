﻿using System.Collections.Generic;

namespace Retoxi
{
    public interface IDrinkParser
    {
        List<Drink> ParseFile(string fileName);
    }
}
