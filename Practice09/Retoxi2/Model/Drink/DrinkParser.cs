﻿using System.Collections.Generic;
using System.IO;

namespace Retoxi
{
    class DrinkParser : IDrinkParser
    {
        public List<Drink> ParseFile(string fileName)
        {
            List<Drink> drinks = new List<Drink>();
            using (StreamReader reader = new StreamReader(fileName))
            {
                string[] dataItems;
                while (!reader.EndOfStream)
                {
                    dataItems = reader.ReadLine().Split(';');
                    drinks.Add(
                        new Drink(
                            dataItems[0],
                            int.Parse(dataItems[1])
                        )
                    );
                }
            }
            return drinks;
        }
    }
}
