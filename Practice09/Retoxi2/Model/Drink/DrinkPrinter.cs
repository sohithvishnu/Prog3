﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Retoxi
{
    class DrinkPrinter
    {
        public static string PrintToPriceList(Drink item)
        {
            return item.Name + " (" + item.UnitPrice + " Ft)";
        }

        public static string PrintToInvoice(Drink item)
        {
            return item.UnpayedOrder.ToString().PadLeft(4) + " " + item.Name.PadRight(34)
                + item.OrderPrice.ToString().PadLeft(10) + " Ft";
        }

        public static string PrintToInventory(Drink item)
        {
            return "  " + item.Name.PadRight(33) + ": "
                + item.PayedOrder.ToString().PadLeft(6) + item.UnpayedOrder.ToString().PadLeft(7);
        }

        public static string PrintListToText(IEnumerable<Drink> drinks, Func<Drink, string> printer)
        {
            StringBuilder sb = new StringBuilder();

            sb.AppendLine("Eladott italadagok:");
            foreach (Drink drink in drinks)
            {
                sb.AppendLine(printer(drink));
            }

            return sb.ToString();
        }

        public static void PrintListToFile(IEnumerable<Drink> drinks, string fileName, Func<Drink, string> printer)
        {
            using (StreamWriter writer = new StreamWriter(fileName))
            {
                writer.WriteLine("Eladott italadagok:");

                writer.WriteLine(PrintListToText(drinks, printer));

                //foreach (Drink drink in drinks)
                //{
                //    writer.WriteLine(printer(drink));
                //}
            }
        }
    }
}
