﻿using System.Collections;
using System.Collections.Generic;

namespace Retoxi
{
    class DrinkStorageEnumerator : IEnumerator<Drink>
    {
        private DrinkStorage storage;
        private int index;

        public DrinkStorageEnumerator(DrinkStorage storage)
        {
            this.storage = storage;
            index = -1;
        }

        public Drink Current => storage[index];

        object IEnumerator.Current => Current;

        public void Dispose()
        { }

        public bool MoveNext()
        {
            return ++index < storage.Count;
        }

        public void Reset()
        {
            index = -1;
        }
    }
}
