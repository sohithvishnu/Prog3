﻿using System.Drawing;
using System.Windows.Forms;

namespace Retoxi
{
    class DrinkMenuControl
    {
        public Drink Model { get; private set; }
        private CheckBox name;
        private TextBox pieces;

        public bool Checked { get { return name.Checked; } }

        public string Pieces { get { return pieces.Text; } }

        private bool hasError;
        public bool HasError {
            get { return hasError; }
            set {
                hasError = value;
                pieces.BackColor = hasError ? Color.Salmon : Color.White;
            }
        }

        public DrinkMenuControl(Drink model, CheckBox name, TextBox pieces)
        {
            Model = model;
            this.name = name;
            this.pieces = pieces;
            hasError = false;
        }

        public void Reset()
        {
            name.Checked = false;
            pieces.Clear();
            HasError = false;
        }
    }
}
