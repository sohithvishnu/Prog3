﻿using System;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Threading;

namespace Database
{
    class DBManager
    {
        IDbConnector connector;

        public DBManager(IDbConnector connector)
        {
            this.connector = connector;
        }

        public void CreateTable()
        {
            connector.Open();
            connector.GetCreateCommand().ExecuteNonQuery();
            connector.Close();
        }

        public void LoadFile(
            string fileName, 
            Action<int> progressReporter
        )
        {
            progressReporter(0);

            connector.Open();

            progressReporter(10);

            if (fileName.Length > 0)
            {
                LoadFile(fileName, ';', connector.GetInsertCommand(), progressReporter);
            }

            progressReporter(90);

            connector.Close();

            progressReporter(100);
        }

        public DataTable ReadData()
        {
            connector.Open();
            DbDataReader reader = connector.GetSelectAllCommand().ExecuteReader();
            DataTable personTable = new DataTable();
            personTable.Load(reader);
            connector.Close();
            return personTable;
        }

        private void LoadFile(string fileName, char valueSeparator, DbCommand command, Action<int> progressReporter)
        {
            using (StreamReader reader = new StreamReader(fileName))
            {
                int lineCount = 0;

                while (!reader.EndOfStream)
                {
                    lineCount++;
                    string[] values = reader.ReadLine().Split(valueSeparator);

                    if (values.Length == 3)
                    {
                        command.Parameters["@first_name"].Value = values[0];
                        command.Parameters["@last_name"].Value = values[1];
                        SetIntFied(fileName, lineCount, command, "salary", values[2]);

                        command.ExecuteNonQuery();
                    }

                    Thread.Sleep(200);

                    progressReporter(10 + lineCount);
                }
            }
        }

        #region Value converters

        private void SetIntFied(string fileName, int lineCount, DbCommand command, string fieldName, string value)
        {
            try
            {
                command.Parameters["@" + fieldName].Value = int.Parse(value);
            }
            catch (Exception ex)
            {
                throw new FileParseException(fileName, lineCount, fieldName, value, "File parse error", ex);
            }
        }

        #endregion
    }
}
