﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database
{
    class OperationParameters
    {
        public IDbConnector Connection { get; private set; }

        public string FileName { get; private set; }

        public OperationParameters(
            IDbConnector connection, string fileName)
        {
            Connection = connection;
            FileName = fileName;
        }
    }
}
