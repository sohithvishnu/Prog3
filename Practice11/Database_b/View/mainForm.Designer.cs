﻿namespace Database
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.useMSSql = new System.Windows.Forms.RadioButton();
            this.useSQLite = new System.Windows.Forms.RadioButton();
            this.sqliteConnectionString = new System.Windows.Forms.TextBox();
            this.msSqlConnctionString = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.testConnection = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.fileName = new System.Windows.Forms.TextBox();
            this.browseFile = new System.Windows.Forms.Button();
            this.createTable = new System.Windows.Forms.Button();
            this.insertFile = new System.Windows.Forms.Button();
            this.readTable = new System.Windows.Forms.Button();
            this.dataList = new System.Windows.Forms.ListBox();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.backgroundWorker = new System.ComponentModel.BackgroundWorker();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.progressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.name = new System.Windows.Forms.TextBox();
            this.salary = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // useMSSql
            // 
            this.useMSSql.AutoSize = true;
            this.useMSSql.Location = new System.Drawing.Point(12, 32);
            this.useMSSql.Name = "useMSSql";
            this.useMSSql.Size = new System.Drawing.Size(164, 21);
            this.useMSSql.TabIndex = 0;
            this.useMSSql.Text = "Microsoft SQL Server";
            this.useMSSql.UseVisualStyleBackColor = true;
            this.useMSSql.CheckedChanged += new System.EventHandler(this.connectionType_CheckedChanged);
            // 
            // useSQLite
            // 
            this.useSQLite.AutoSize = true;
            this.useSQLite.Location = new System.Drawing.Point(12, 60);
            this.useSQLite.Name = "useSQLite";
            this.useSQLite.Size = new System.Drawing.Size(135, 21);
            this.useSQLite.TabIndex = 1;
            this.useSQLite.Text = "SQLite database";
            this.useSQLite.UseVisualStyleBackColor = true;
            this.useSQLite.CheckedChanged += new System.EventHandler(this.connectionType_CheckedChanged);
            // 
            // sqliteConnectionString
            // 
            this.sqliteConnectionString.Enabled = false;
            this.sqliteConnectionString.Location = new System.Drawing.Point(182, 59);
            this.sqliteConnectionString.Name = "sqliteConnectionString";
            this.sqliteConnectionString.Size = new System.Drawing.Size(583, 22);
            this.sqliteConnectionString.TabIndex = 2;
            this.sqliteConnectionString.Text = "URI=file:sqlite.db";
            // 
            // msSqlConnctionString
            // 
            this.msSqlConnctionString.Enabled = false;
            this.msSqlConnctionString.Location = new System.Drawing.Point(182, 31);
            this.msSqlConnctionString.Name = "msSqlConnctionString";
            this.msSqlConnctionString.Size = new System.Drawing.Size(583, 22);
            this.msSqlConnctionString.TabIndex = 3;
            this.msSqlConnctionString.Text = "Data Source=server; Initial Catalog=database; User Id=userName; Password=password" +
    "";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(182, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(583, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "ConnectionString";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // testConnection
            // 
            this.testConnection.Location = new System.Drawing.Point(320, 87);
            this.testConnection.Name = "testConnection";
            this.testConnection.Size = new System.Drawing.Size(147, 29);
            this.testConnection.TabIndex = 5;
            this.testConnection.Text = "Test connection";
            this.testConnection.UseVisualStyleBackColor = true;
            this.testConnection.Click += new System.EventHandler(this.testConnection_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "File name:";
            // 
            // fileName
            // 
            this.fileName.Location = new System.Drawing.Point(182, 151);
            this.fileName.Name = "fileName";
            this.fileName.Size = new System.Drawing.Size(549, 22);
            this.fileName.TabIndex = 7;
            // 
            // browseFile
            // 
            this.browseFile.Location = new System.Drawing.Point(732, 150);
            this.browseFile.Name = "browseFile";
            this.browseFile.Size = new System.Drawing.Size(33, 27);
            this.browseFile.TabIndex = 8;
            this.browseFile.Text = "...";
            this.browseFile.UseVisualStyleBackColor = true;
            this.browseFile.Click += new System.EventHandler(this.browseFile_Click);
            // 
            // createTable
            // 
            this.createTable.Location = new System.Drawing.Point(23, 192);
            this.createTable.Name = "createTable";
            this.createTable.Size = new System.Drawing.Size(167, 45);
            this.createTable.TabIndex = 9;
            this.createTable.Text = "Create table";
            this.createTable.UseVisualStyleBackColor = true;
            this.createTable.Click += new System.EventHandler(this.createTable_Click);
            // 
            // insertFile
            // 
            this.insertFile.Location = new System.Drawing.Point(310, 192);
            this.insertFile.Name = "insertFile";
            this.insertFile.Size = new System.Drawing.Size(167, 45);
            this.insertFile.TabIndex = 10;
            this.insertFile.Text = "Insert file content";
            this.insertFile.UseVisualStyleBackColor = true;
            this.insertFile.Click += new System.EventHandler(this.insertFile_Click);
            // 
            // readTable
            // 
            this.readTable.Location = new System.Drawing.Point(597, 192);
            this.readTable.Name = "readTable";
            this.readTable.Size = new System.Drawing.Size(167, 45);
            this.readTable.TabIndex = 11;
            this.readTable.Text = "Read table content";
            this.readTable.UseVisualStyleBackColor = true;
            this.readTable.Click += new System.EventHandler(this.readTable_Click);
            // 
            // dataList
            // 
            this.dataList.FormattingEnabled = true;
            this.dataList.ItemHeight = 16;
            this.dataList.Location = new System.Drawing.Point(23, 243);
            this.dataList.Name = "dataList";
            this.dataList.Size = new System.Drawing.Size(230, 196);
            this.dataList.TabIndex = 12;
            this.dataList.SelectedIndexChanged += new System.EventHandler(this.dataList_SelectedIndexChanged);
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            this.dataGridView.AllowUserToOrderColumns = true;
            this.dataGridView.AllowUserToResizeColumns = false;
            this.dataGridView.AllowUserToResizeRows = false;
            this.dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridView.Location = new System.Drawing.Point(310, 243);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.RowTemplate.Height = 24;
            this.dataGridView.Size = new System.Drawing.Size(453, 306);
            this.dataGridView.TabIndex = 13;
            // 
            // backgroundWorker
            // 
            this.backgroundWorker.WorkerReportsProgress = true;
            this.backgroundWorker.WorkerSupportsCancellation = true;
            this.backgroundWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_DoWork);
            this.backgroundWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_ProgressChanged);
            this.backgroundWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_RunWorkerCompleted);
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.progressBar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 552);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(779, 25);
            this.statusStrip1.TabIndex = 14;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // progressBar
            // 
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(100, 19);
            this.progressBar.Visible = false;
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(53, 20);
            this.statusLabel.Text = "Ready.";
            // 
            // name
            // 
            this.name.Enabled = false;
            this.name.Location = new System.Drawing.Point(23, 466);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(230, 22);
            this.name.TabIndex = 15;
            // 
            // salary
            // 
            this.salary.Enabled = false;
            this.salary.Location = new System.Drawing.Point(23, 514);
            this.salary.Name = "salary";
            this.salary.Size = new System.Drawing.Size(230, 22);
            this.salary.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 446);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 17);
            this.label3.TabIndex = 17;
            this.label3.Text = "Name:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 494);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 17);
            this.label4.TabIndex = 18;
            this.label4.Text = "Salary:";
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(779, 577);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.salary);
            this.Controls.Add(this.name);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.dataGridView);
            this.Controls.Add(this.dataList);
            this.Controls.Add(this.readTable);
            this.Controls.Add(this.insertFile);
            this.Controls.Add(this.createTable);
            this.Controls.Add(this.browseFile);
            this.Controls.Add(this.fileName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.testConnection);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.msSqlConnctionString);
            this.Controls.Add(this.sqliteConnectionString);
            this.Controls.Add(this.useSQLite);
            this.Controls.Add(this.useMSSql);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Database connection";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton useMSSql;
        private System.Windows.Forms.RadioButton useSQLite;
        private System.Windows.Forms.TextBox sqliteConnectionString;
        private System.Windows.Forms.TextBox msSqlConnctionString;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button testConnection;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox fileName;
        private System.Windows.Forms.Button browseFile;
        private System.Windows.Forms.Button createTable;
        private System.Windows.Forms.Button insertFile;
        private System.Windows.Forms.Button readTable;
        private System.Windows.Forms.ListBox dataList;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.ComponentModel.BackgroundWorker backgroundWorker;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.ToolStripProgressBar progressBar;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox salary;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

