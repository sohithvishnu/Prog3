﻿using System;
using System.Data;
using System.Windows.Forms;

namespace Database
{
    public partial class mainForm : Form
    {
        private bool operationRunnung;

        public mainForm()
        {
            InitializeComponent();
            operationRunnung = false;
        }

        private void connectionType_CheckedChanged(object sender, EventArgs e)
        {
            msSqlConnctionString.Enabled = useMSSql.Checked;
            sqliteConnectionString.Enabled = useSQLite.Checked;
        }

        private IDbConnector GetConnectorBySelection()
        {
            return useMSSql.Checked
                    ? (IDbConnector)new MsSqlConnector(msSqlConnctionString.Text)
                    : (IDbConnector)new SQLiteConnector(sqliteConnectionString.Text);
        }

        private DBManager GetManager()
        {
            IDbConnector connector = GetConnectorBySelection();
            return new DBManager(connector);
        }

        private void testConnection_Click(object sender, EventArgs e)
        {
            try
            {
                IDbConnector connector = GetConnectorBySelection();

                connector.Open();
                connector.Close();

                MessageBox.Show("Connection OK!", "Test connection");
            }
            catch(Exception ex)
            {
                MessageBox.Show(
                    ex.Message,
                    "Error occurred",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );
            }
        }

        private void createTable_Click(object sender, EventArgs e)
        {
            if (!operationRunnung)
            {
                try
                {
                    DBManager manager = GetManager();

                    manager.CreateTable();

                    MessageBox.Show("Table has been created!", "DB operation");
                }catch(Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error occurred");
                }
            }
            else
            {
                MessageBox.Show("Operation is running", "Attention");
            }
        }

        private void browseFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();

            ofd.FileName = fileName.Text;
            if(ofd.ShowDialog() == DialogResult.OK)
            {
                fileName.Text = ofd.FileName;
            }
        }

        private void insertFile_Click(object sender, EventArgs e)
        {
            if (!operationRunnung)
            {
                operationRunnung = true;

                OperationParameters op = new OperationParameters(
                    GetConnectorBySelection(),
                    fileName.Text
                );

                progressBar.Value = 0;
                progressBar.Visible = true;
                backgroundWorker.RunWorkerAsync(op);
            }
            else
            {
                MessageBox.Show("Operation is running", "Attention");
            }
        }

        private void readTable_Click(object sender, EventArgs e)
        {
            DBManager manager = GetManager();

            DataTable table = manager.ReadData();

            dataList.DataSource = table;
            dataList.DisplayMember = "last_name";

            dataGridView.DataSource = table;
        }

        private void dataList_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRowView dataRow = (DataRowView)dataList.SelectedItem;
            name.Text = string.Format("{0} {1}", dataRow[1], dataRow[2]);
            salary.Text = dataRow[3].ToString();
        }

        #region BackgroundWorker event handlers

        private void backgroundWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            OperationParameters parameters = (OperationParameters)e.Argument;

            DBManager manager = new DBManager(parameters.Connection);

            manager.LoadFile(fileName.Text, backgroundWorker.ReportProgress);
        }

        private void backgroundWorker_ProgressChanged(object sender, System.ComponentModel.ProgressChangedEventArgs e)
        {
            progressBar.Value = e.ProgressPercentage;
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, System.ComponentModel.RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled == true)
            {
                statusLabel.Text = "Canceled!";
            }
            else if (e.Error != null)
            {
                statusLabel.Text = "Error: " + e.Error.Message;
            }
            else
            {
                statusLabel.Text = "Ready.";
            }

            progressBar.Visible = false;

            operationRunnung = false;
        }


        #endregion
    }
}
