﻿using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Database
{
    class MsSqlConnector : IDbConnector
    {
        const string CREATE_TABLE = "CREATE TABLE[dbo].[person] (id int IDENTITY(1,1) NOT NULL, first_name varchar(30) NOT NULL, last_name varchar(30) NOT NULL, salary int NOT NULL)";

        SqlConnection connection;

        public MsSqlConnector(string connectionString)
        {
            connection = new SqlConnection(connectionString);
        }

        public void Open()
        {
            connection.Open();
        }

        public void Close()
        {
            connection.Close();
        }

        public DbCommand GetCreateCommand()
        {
            SqlCommand command = new SqlCommand(CREATE_TABLE, connection);

            return command;
        }

        public DbCommand GetInsertCommand()
        {
            SqlCommand command = new SqlCommand(
                "INSERT INTO person " +
                "(first_name, last_name, salary)" +
                " values " +
                "(@first_name, @last_name, @salary)",
                connection
            );

            command.Parameters.Add("@first_name", SqlDbType.VarChar);
            command.Parameters.Add("@last_name", SqlDbType.VarChar);
            command.Parameters.Add("@salary", SqlDbType.Int);

            command.Prepare();

            return command;
        }

        public DbCommand GetSelectAllCommand()
        {
            SqlCommand command = new SqlCommand(
                "SELECT * FROM person",
                connection
            );

            return command;
        }
    }
}
