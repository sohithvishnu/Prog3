﻿using System.Data;
using System.Data.Common;
using System.Data.SQLite;

//Right mouse click on project in Solution Explorer
//Select Manage NuGet Packages...
//Browse for System.Data.Sqlite package and install it with prerequisites

namespace Database
{
    class SQLiteConnector : IDbConnector
    {
        const string CREATE_TABLE = "CREATE TABLE \"person\" (\"id\" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, \"first_name\" TEXT NOT NULL, \"last_name\" TEXT NOT NULL, \"salary\" INTEGER NOT NULL)";

        SQLiteConnection connection;

        public SQLiteConnector(string connectionString)
        {
            connection = new SQLiteConnection(connectionString);
        }

        public void Open()
        {
            connection.Open();
        }

        public void Close()
        {
            connection.Close();
        }

        public DbCommand GetCreateCommand()
        {
            SQLiteCommand command = new SQLiteCommand(CREATE_TABLE, connection);

            return command;
        }

        public DbCommand GetInsertCommand()
        {
            SQLiteCommand command = new SQLiteCommand(
                "INSERT INTO person " +
                "(first_name, last_name, salary)" +
                " values " +
                "(@first_name, @last_name, @salary)",
                connection
            );

            command.Parameters.Add(new SQLiteParameter("@first_name", DbType.String));
            command.Parameters.Add(new SQLiteParameter("@last_name", DbType.String));
            command.Parameters.Add(new SQLiteParameter("@salary", DbType.Int32));

            command.Prepare();

            return command;
        }

        public DbCommand GetSelectAllCommand()
        {
            SQLiteCommand command = new SQLiteCommand(
                "SELECT * FROM person",
                connection
            );

            return command;
        }
    }
}
