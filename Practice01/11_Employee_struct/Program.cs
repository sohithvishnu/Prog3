﻿using System;

namespace _11_Employee_struct
{
    class Program
    {
        private const int WORK_HOUR_MIN = 1;
        private const int WORK_HOUR_MAX = 24;
        private const int HOURLY_WAGE_MIN = 100;
        private const int HOURLY_WAGE_MAX = 5000;

        static void Main(string[] args)
        {
            int total = 0;
            Employee employee;
            ConsoleKeyInfo keyInfo;

            do
            {
                employee = GetEmployeeFromConsole();

                //Print employee data
                Console.WriteLine("{0}", employee);

                //Calculate total wage to pay
                total += employee.CalculateWage();

                Console.Write("Is there new employee? (y/n) ");
                keyInfo = Console.ReadKey();
                Console.WriteLine();

                //Waiting for single key press
                //Convert to lowercase
                //Continue iteration when 'y' entered
            } while (char.ToLower(keyInfo.KeyChar) == 'y');

            Console.WriteLine("Total wage: {0}", total);
        }

        private static int GetIntFromConsole(string message, int min, int max)
        {
            int number;
            Console.Write(message);
            while (!int.TryParse(Console.ReadLine(), out number) || number < min || number > max)
            {
                Console.WriteLine("Please enter an integer number ({0}-{1})!", min, max);
            }
            return number;
        }

        private static Employee GetEmployeeFromConsole()
        {
            Employee employee;          //On default initialization, no need for new operator
                                        //value type variable has been created

            Console.Write("Employee name: ");
            employee.Name = Console.ReadLine();
            employee.WorkedHours = GetIntFromConsole("Number of worked hours: ", WORK_HOUR_MIN, WORK_HOUR_MAX);
            employee.HourlyWage = GetIntFromConsole("Hourly wage: ", HOURLY_WAGE_MIN, HOURLY_WAGE_MAX);

            return employee;
        }

        private static Employee2 GetEmployee2FromConsole()
        {
            Console.Write("Employee name: ");
            string name = Console.ReadLine();

            return new Employee2(
                name,
                GetIntFromConsole("Number of worked hours: ", WORK_HOUR_MIN, WORK_HOUR_MAX),
                GetIntFromConsole("Hourly wage: ", HOURLY_WAGE_MIN, HOURLY_WAGE_MAX)
            );
        }
    }
}
