﻿using System;

namespace _11_Employee_struct
{
    struct Employee2
    {
        public string Name;
        public int WorkedHours;
        public int HourlyWage;

        public Employee2(string name, int hours, int wage)
        {
            Name = name;
            WorkedHours = hours;
            HourlyWage = wage;
        }

        public int CalculateWage()
        {
            return WorkedHours * HourlyWage;
        }

        public override string ToString()
        {
            return String.Format("{0} wage: {1}HUF", Name, CalculateWage());
        }
    }
}
