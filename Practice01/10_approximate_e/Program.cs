﻿using System;

namespace _10_approximate_e
{
    class Program
    {
        static void Main(string[] args)
        {
            const double eta = 0.0001;

            double factorial = 1;
            double n = 0;
            double increment = 1;
            double eApproximation = 1;

            //Calculates factorial and uses that to compute current value of the Euler's number.
            //  while the reciprocal of the factorial is the inrement of e, 
            //  when the increment is less than the approximation error (eta)
            //  the approximation is accurate enough
            while(increment >= eta)
            {
                factorial *= ++n;
                increment = 1.0 / factorial;
                eApproximation += increment;
            }

            Console.WriteLine(
                String.Format(
                    "Approximation of e in {0} steps with {1} accuracy: {2}", 
                    n, eta, eApproximation
                )
            );
        }
    }
}
