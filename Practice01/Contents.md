# Contents of Practice 01  
In this lecture you see how to:
- basic structure of a .Net Console application
- declare variables, sinple types
- declare and initialize structures
- declare methods and their parameters
  
## 01 - HelloWorld  
Introduces the basic structure of a C# console application.  
  
## 02 - Divisor 
*Practice basic control statements:* `sequence, selection`  
Check if an entered number can be divided by other numers.  
As an extension, check if an entered number is a prime number.  
  
## 03 - Signum  
*Practice basic control statements:* `sequence, selection`  
Check the signum of an entered number.
  
## 05 - Factorial
*Practice basic control statements:* `sequence, selection, iteration`  
Compute the factorial of an entered number.  
n! = 1*2*3*...*n
  
## 07 - Fibonacci  
*Practice basic control statements:* `sequence, selection, iteration`  
Compute the n^th^ Fibonacci number.  
F~0~ = 0  
F~1~ = 1  
F~n~ = F~n-1~ + F~n-2~  where n > 1
  
## 10 - Approximate Euler's number  
*Practice basic control statements:* `sequence, selection, iteration`  
Approximate the Euler's number by calculating the formula until entered number (n).  
e = lim~n->oo~(1+1/n!)^n^ = **sum~0..n~(1/n!)**  
e ~ 2.7182818284590452353602874713526624977572470936999595749669676277240766   
  
## 11 - Use of C like structures in C#
### A. Introduce C# structures
Presentation how to create a structure with visibility modifiers and constructor.  
Demonstrates how value type variables work.
  
### B. Example of using structure
Store current and selected employees in structure variables.

## 12 - RunLengthEncoding - RLE
RLE is a character list encoding. Using this, the string could be represented on less storage space.  
  
The main idea is that, repeated characters are represented by the character and the repetition count.  
  
Difficulties:
- there are only few repetitions in the string  
- encoded repetitions could be marked. For this, a special character is required.
- when repetition count is 2 or 3, this encoding is not effective
  
Try to get rid of these difficulties and create an application which always return not-longer representation!  
  
Test with these strings:
- CCCCCDDDAAAAAAAAAABBBBDCCCBBBDDDDDDCCCCCCCCDDDDDAAADDDAAAAAABBDDCABBDDBBB
- ABBBCDDBDBCCCCBAACDACCBDACCCDBCABBBCDCDBBACDBDBCBACDACBDDACDDDBCAAABCDCDB
- ABCDBDBCBACDACBDACDBCABCDCDBACDBDBCBACDACBDACDBCABCDCDBACDBDBCBACDACBDACD

