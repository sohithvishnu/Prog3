﻿using System;

namespace _07_Fibonacci
{
    class Program
    {
        const int MAX_COUNT = 30;

        static void Main(string[] args)
        {
            int count = GetIntFromConsole(
                String.Format("Which Fibonacci number do you want to compute?\nEnter its index (1-{0})?", MAX_COUNT),
                1, MAX_COUNT
            );

            Console.WriteLine("Computed by cycle: {0}", FibonacciCycle(count));

            Console.WriteLine("Computed by recursion: {0}", FibonacciRecursive(count));
        }

        private static int GetIntFromConsole(string message, int min, int max)
        {
            int count;
            Console.WriteLine(message);
            while (!int.TryParse(Console.ReadLine(), out count) || count < min || count > max)
            {
                Console.WriteLine("Please try again!");
            }
            return count;
        }

        //Calculate Fibonacci number by iteration
        private static int FibonacciCycle(int n)
        {
            int f = 0;
            switch (n)
            {
                case 1:
                    f = 0;
                    break;
                case 2:
                    f = 1;
                    break;
                default:
                    int fm2 = 0;
                    int fm1 = 1;
                    f = fm2 + fm1;  //third Fibonacci number, computed directly
                    for (int i = 4; i <= n; i++)
                    {
                        fm2 = fm1;
                        fm1 = f;
                        f = fm2 + fm1;
                    }
                    break;
            }

            return f;
        }

        //Calculate Fibonacci number by recursion
        //Method calls itself (that is named: recursion) to compute a part of the answer
        // all meghod calls add content to stack, so recursion is limited by stack size
        private static int FibonacciRecursive(int n)
        {
            if (n == 1) return 0;
            if (n == 2) return 1;

            return FibonacciRecursive(n - 1) + FibonacciRecursive(n - 2);
        }
    }
}
