﻿using System;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)         //Main method (with string[] argument) is required as execution entry point
        {
            Console.WriteLine("Hello world");   //Writing data to console (with a new line after)
        }
    }
}
