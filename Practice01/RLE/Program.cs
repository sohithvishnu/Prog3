﻿using System;

namespace RLE
{
    class Program
    {
        static void Main(string[] args)
        {
            // Python code to generate string: 
            //    "".join([c * n for c, n in list(zip(sample(["A", "B", "C", "D"]*20,30), list(random.randint(1, 5, size=(30)))))])

            string originalContent = "CCCCCDDDAAAAAAAAAABBBBDCCCBBBDDDDDDCCCCCCCCDDDDDAAADDDAAAAAABBDDCABBDDBBB";
            string marker = "#";

            Console.WriteLine(String.Format("Original: [{0}] {1}", originalContent, originalContent.Length));

            string encodedWithMarker = EncodeWithMarker(originalContent, marker);
            Console.WriteLine(String.Format("Encoded with marker: [{0}] {1}", encodedWithMarker.Length, encodedWithMarker));

            Console.WriteLine(String.Format("Decoded with marker: {0}", DecodeWithMarker(encodedWithMarker, marker)));

            string encodedWithoutMarker = EncodeWithoutMarker(originalContent);
            Console.WriteLine(String.Format("Encoded without marker: [{0}] {1}", encodedWithoutMarker.Length, encodedWithoutMarker));

            Console.WriteLine(String.Format("Decoded without marker: {0}", DecodeWithoutMarker(encodedWithoutMarker)));
        }

        static string EncodeWithMarker(string original, string marker)
        {
            return "";
        }

        static string DecodeWithMarker(string encoded, string marker)
        {
            return "";
        }

        static string EncodeWithoutMarker(string original)
        {
            return "";
        }

        static string DecodeWithoutMarker(string encoded)
        {
            return "";
        }
    }
}
