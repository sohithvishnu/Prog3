﻿using System.Collections;
using System.Collections.Generic;

namespace CarRent
{
    class VehicleCatalog : ICatalog<Vehicle>
    {
        public enum Commands
        {
            Rent,
            Return,
            Transport,
            Scrap
        }

        public static bool CheckLicenseFormat(Vehicle vehicle)
        {
            if (vehicle.Licence.Length != 7 || vehicle.Licence[3] != '-')
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public delegate bool Command(Vehicle license);

        private List<Vehicle> vehicles;

        public VehicleCatalog(List<Vehicle> vehicles, int defaultFare)
        {
            //Initialize generic data of all Vehicles
            Vehicle.DefaultFare = defaultFare;

            //Initialize vhicle storage
            this.vehicles = new List<Vehicle>();
            this.vehicles.AddRange(vehicles);
        }

        #region Find

        public Vehicle Find(string licence)
        {
            foreach(Vehicle v in vehicles)
            {
                if(v.Licence == licence)
                {
                    return v;
                }
            }

            return null;
        }

        //Create indexer for accessing item
        public Vehicle this[string i]
        {
            get
            {
                return Find(i);
            }
        }

        public List<Vehicle> FindBusWithSeats(int requiredSeatCount)
        {
            List<Vehicle> matches = new List<Vehicle>();
            foreach (Vehicle v in vehicles) {
                Bus bus = v as Bus;
                if (bus != null)
                {
                    if(bus.SeatCount >= requiredSeatCount)
                    {
                        matches.Add(bus);
                    }
                }
            }

            return matches;
        }

        public List<Vehicle> FindTruckWithWeight(double requiredWeight)
        {
            List<Vehicle> matches = new List<Vehicle>();
            foreach (Vehicle v in vehicles)
            {
                Truck truck = v as Truck;
                if (truck != null)
                {
                    if (truck.MaxWeight >= requiredWeight)
                    {
                        matches.Add(truck);
                    }
                }
            }

            return matches;
        }

        #endregion

        #region Change state of vehicles in list

        public bool Rent(string licence)
        {
            return Rent(Find(licence));
        }

        public bool Rent(Vehicle vehicle)
        {
            return vehicle != null && vehicle.Rent();
        }

        public bool Transport(string licence)
        {
            return Transport(Find(licence));
        }

        public bool Transport(Vehicle vehicle)
        {
            return vehicle != null && vehicle.Transport();
        }

        public bool Return(string licence)
        {
            return Return(Find(licence));
        }

        public bool Return(Vehicle vehicle)
        {
            return vehicle != null && vehicle.Return();
        }

        #endregion

        #region ICatalog interface implementation

        public Vehicle this[int i]
        {
            get
            {
                return vehicles[i];
            }
        }

        public int Count
        {
            get { return vehicles.Count; }
        }

        //Add new vehicle
        public void Add(Vehicle newVehicle)
        {
            vehicles.Add(newVehicle);
        }

        #endregion

        //Remove vehicle
        public bool Scrap(string licence)
        {
            return vehicles.Remove(Find(licence));
        }

        #region Single vehicle operation implementations

        //Implementation with enumerated commands
        public bool Operation(string licence, Commands operation)
        {
            if(operation == Commands.Scrap)
            {
                return Scrap(licence);
            }

            Vehicle found = Find(licence);
            if (found != null)
            {
                switch (operation)
                {
                    case Commands.Rent:
                        return found.Rent();
                    case Commands.Transport:
                        return found.Transport();
                    case Commands.Return:
                        return found.Return();
                }
            }
            return false;
        }

        //Implementation with command method reference
        //When not implementing all possible command options
        public bool Operation(string licence, Command command)
        {
            return command(Find(licence));
        }

        #endregion

        #region IEnumerable interface implementation

        //IEnumerable interface implementation requires IEnumerator implementation

        public IEnumerator GetEnumerator()
        {
            return new VehicleCatalogEnum(this);
        }

        #endregion
    }


}
