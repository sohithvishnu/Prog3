﻿using System;

namespace CarRent
{
    abstract class Vehicle : IRentable
    {
        //Enumerated list of possible vehicle statuses
        public enum Statuses
        {
            Available,
            Rented,
            InTransport
        }

        public static int DefaultFare { get; set; }  = 20000;

        public string Licence { get; private set; }
        public int ProdYear { get; private set; }
        public string Id { get; private set; }

        public int Kms { get; set; }
        public Statuses Status { get; private set; }

        public Vehicle(string licence, int prodYear, string id, string state)
        {
            Licence = licence;
            ProdYear = prodYear;
            Id = id;

            //Enum string can be converted to enum value 
            //State string has been received, converted to item of the specified type, then casted to it.
            //Cast is required, because Enum.Parse is a generic method, returns instance of Object class (base of everything)
            Status = (Statuses)Enum.Parse(typeof(Statuses), state);
        }

        #region IRentable interface implementation

        virtual public int Fare
        {
            get
            {
                return DefaultFare;
            }
        }

        public bool IsAvailable
        {
            get
            {
                return Status == Statuses.Available;
            }
        }

        public bool Rent()
        {
            if(Status == Statuses.Available)
            {
                Status = Statuses.Rented;
                return true;
            }
            return false;
        }

        public bool Return()
        {
            if (Status == Statuses.Available)
            {
                return false;
            }

            Status = Statuses.Available;
            return true;
        }

        #endregion

        public bool Transport()
        {
            if (Status == Statuses.Available)
            {
                Status = Statuses.InTransport;
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            //Enum value can be converted to string (Status is inserted into a string)
            return String.Format("Azonosító: {0}, rendszám: {1}, státusz: {2}", Id, Licence, Status);
        }
    }
}
