﻿using System.Collections;

namespace CarRent
{
    //Required by IEnumerable interface implementation in VehicleCatalog
    class VehicleCatalogEnum : IEnumerator
    {
        private int position;
        private VehicleCatalog catalog;

        public VehicleCatalogEnum(VehicleCatalog catalog)
        {
            this.catalog = catalog;
            position = 0;
        }

        #region IEnumerator interface implementations

        public object Current => catalog[position];

        public bool MoveNext()
        {
            position++;
            return (position < catalog.Count);
        }

        public void Reset()
        {
            position = 0;
        }

        #endregion
    }
}
