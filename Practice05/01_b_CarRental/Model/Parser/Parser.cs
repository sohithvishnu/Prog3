﻿using System.Collections.Generic;

namespace CarRent.Model
{
    abstract class Parser
    {
        public abstract List<Vehicle> Parse(string sourceName);
    }
}
