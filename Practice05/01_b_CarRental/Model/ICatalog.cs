﻿using System.Collections;

namespace CarRent
{
    //A generic interface with type parameter
    //derived from IEnumerable interface
    //IEnumrable is implemented in descendant classes
    interface ICatalog<T> : IEnumerable
    {
        //Declare an indexer for accessing items
        T this[int i] { get; }

        //A read-only property to read number of items in collection
        int Count { get; }

        //Add new item to collection
        void Add(T newItem);
    }
}
