﻿using System.Collections;
using System.Collections.Generic;

namespace CarRent
{
    class RentableCatalog : ICatalog<IRentable>
    {
        private List<IRentable> rentables;

        public RentableCatalog()
        {
            rentables = new List<IRentable>();
        }

        #region ICatalog interface implementations

        public int Count {
            get { return rentables.Count; }
        }

        public IRentable this[int i]
        {
            get
            {
                return rentables[i];
            }
        }

        public void Add(IRentable newRentable)
        {
            rentables.Add(newRentable);
        }

        #endregion

        #region IEnumerable interface implementation

        //IEnumerable interface implementation requires IEnumerator implementation

        public IEnumerator GetEnumerator()
        {
            return new RentableCatalogEnum(this);
        }

        #endregion
    }
}
