﻿using System.Collections;

namespace CarRent
{
    //Required by IEnumerable interface implementation in RentableCatalog
    class RentableCatalogEnum : IEnumerator
    {
        private int position;
        private RentableCatalog catalog;

        public RentableCatalogEnum(RentableCatalog catalog)
        {
            this.catalog = catalog;
            Reset();
        }

        #region IEnumerator interface implementations

        public object Current => catalog[position];

        public bool MoveNext()
        {
            position++;
            return (position < catalog.Count);
        }

        public void Reset()
        {
            position = -1;
        }

        #endregion
    }
}
