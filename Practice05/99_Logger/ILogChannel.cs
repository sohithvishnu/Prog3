﻿namespace _99_Logger
{
    interface ILogChannel
    {
        void Write(string message);
    }
}
