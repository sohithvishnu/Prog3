﻿using System;

namespace _99_Logger
{
    class DecoratedMultiChannelLogger: MultiChannelLogger
    {
        private Func<string, LogLevels, string> messageConverter;
        private IMessageDecorator decorator;

        public DecoratedMultiChannelLogger(
            Func<string, LogLevels, string> messageConverter = null,
            IMessageDecorator decorator = null
        )
        {
            this.messageConverter = messageConverter;
            this.decorator = decorator;
        }


        public override void Write(string message, LogLevels messageLevel)
        {
            if (messageLevel <= Level)
            {
                foreach (ILogChannel channel in channels)
                {
                    string messageToLog = message;
                    if (messageConverter != null)
                        messageToLog = messageConverter(message, messageLevel);

                    if (decorator != null)
                        messageToLog = decorator.Decorate(messageToLog);

                    channel.Write(messageToLog);
                }
            }
        }
    }
}
