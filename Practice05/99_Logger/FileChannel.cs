﻿using System.IO;

namespace _99_Logger
{
    class FileChannel : ILogChannel
    {
        public string FileName { get; private set; }

        public FileChannel(string fileName)
        {
            FileName = fileName;
        }

        public void Write(string message)
        {
            using (StreamWriter sw = File.AppendText(FileName))
            {
                sw.WriteLine(message);
            }
        }
    }
}
