﻿namespace _99_Logger
{
    interface IMessageDecorator
    {
        string Decorate(string message);
    }
}
