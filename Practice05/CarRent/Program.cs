﻿using CarRent.Model;
using System;
using System.Collections.Generic;

namespace CarRent
{
    class Program
    {
        const int DEFAULT_FARE = 30000;

        public static void PrintList(List<Vehicle> vehicles, string title)
        {
            Console.WriteLine("----==== {0}", title);
            foreach (var v in vehicles)
            {
                Console.WriteLine(v);
            }
        }

        static void Main(string[] args)
        {
            //Initialize instance of VehicleCatalog from text file
            VehicleCatalog catalog = new VehicleCatalog(
                new FileParser().Parse("..\\..\\jarmuvek.txt"), 
                DEFAULT_FARE,
                Program.PrintList
            );

            //Print list
            catalog.PrintFullList();

            //Print results of a filter
            catalog.PrintBusWithSeats(33);

            //Print results of a filter
            catalog.PrintTruckWithWeight(2);

            //Find
            Console.WriteLine("----==== Find: DRS-374");
            Console.WriteLine(catalog.Find("DRS-374"));

            //Access through indexer
            Console.WriteLine("----==== Index: DRS-374");
            Console.WriteLine(catalog["DRS-374"]);

            //Rent a vehicle
            catalog.Rent("DRS-374");
            Console.WriteLine("----==== Rent: DRS-374");
            Console.WriteLine(catalog.Find("DRS-374"));

            //Return a vehicle
            catalog.Return("DRS-374");
            Console.WriteLine("----==== Return: DRS-374");
            Console.WriteLine(catalog["DRS-374"]);
        }
    }
}
