﻿using System;
using System.Collections.Generic;

namespace CarRent
{
    class VehicleCatalog
    {
        public delegate void VehicleListPrinter(List<Vehicle> list, string title);

        private List<Vehicle> vehicles;
        private VehicleListPrinter listPrinter;

        public VehicleCatalog(List<Vehicle> vehicles, int defaultFare, VehicleListPrinter listPrinterMethod)
        {
            //Initialize generic data of all Vehicles
            Vehicle.DefaultFare = defaultFare;

            //Initialize vhicle storage
            this.vehicles = new List<Vehicle>();
            this.vehicles.AddRange(vehicles);

            listPrinter = listPrinterMethod;
        }

        public void Sort()
        {
            //No need for comparer, because Vehicle class implements IComparable interface
            //Sort method can call its IComparable.CompareTo method to compare elements of the array
            vehicles.Sort();
        }

        #region Find item

        public Vehicle Find(string licence)
        {
            foreach(Vehicle v in vehicles)
            {
                if(v.Licence == licence)
                {
                    return v;
                }
            }

            return null;
        }

        //Create indexer for accessing item
        public Vehicle this[string i]
        {
            get
            {
                return Find(i);
            }
        }

        public Vehicle this[int i]
        {
            get
            {
                return vehicles[i];
            }
        }

        #endregion

        #region Printers (filters)

        public void PrintFullList()
        {
            listPrinter(vehicles, "Full list");
        }

        public void PrintBusWithSeats(int requiredSeatCount)
        {
            List<Vehicle> matches = new List<Vehicle>();
            foreach (Vehicle v in vehicles) {
                Bus bus = v as Bus;
                if (bus != null)
                {
                    if(bus.SeatCount >= requiredSeatCount)
                    {
                        matches.Add(bus);
                    }
                }
            }

            listPrinter(matches, String.Format("Bus with minimum {0} seats", requiredSeatCount));
        }

        public void PrintTruckWithWeight(double requiredWeight)
        {
            List<Vehicle> matches = new List<Vehicle>();
            foreach (Vehicle v in vehicles)
            {
                Truck truck = v as Truck;
                if (truck != null)
                {
                    if (truck.MaxWeight >= requiredWeight)
                    {
                        matches.Add(truck);
                    }
                }
            }

            listPrinter(matches, String.Format("Truck with minimum {0} weight capacity", requiredWeight));
        }

        #endregion

        #region Change state of vehicles in list

        public bool Rent(string licence)
        {
            Vehicle found = Find(licence);
            return found != null && found.Rent();
        }

        public bool Transport(string licence)
        {
            Vehicle found = Find(licence);
            return found != null && found.Transport();
        }

        public bool Return(string licence)
        {
            Vehicle found = Find(licence);
            return found != null && found.Return();
        }

        #endregion

        #region Shop

        //Add new vehicle
        public void Buy(Vehicle newVehicle)
        {
            vehicles.Add(newVehicle);
        }

        //Remove vehicle
        public bool Scrap(string licence)
        {
            return vehicles.Remove(Find(licence));
        }

        #endregion
    }
}
