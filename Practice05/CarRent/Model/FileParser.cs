﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarRent.Model
{
    class FileParser : Parser
    {
        public override List<Vehicle> Parse(string sourceName)
        {
            StreamReader reader = null;
            List<Vehicle> vehicles = new List<Vehicle>();

            try
            {
                reader = new StreamReader(sourceName);
                string type;
                while (!reader.EndOfStream)
                {
                    type = reader.ReadLine();
                    if (type == "busz")
                    {
                        vehicles.Add(
                            new Bus(
                                reader.ReadLine(),
                                Convert.ToInt32(reader.ReadLine()),
                                string.Format("B{0}", vehicles.Count + 1),
                                Convert.ToInt32(reader.ReadLine())
                            )
                        );
                    }
                    else
                    {
                        vehicles.Add(
                            new Truck(
                                reader.ReadLine(),
                                Convert.ToInt32(reader.ReadLine()),
                                string.Format("T{0}", vehicles.Count + 1),
                                Convert.ToDouble(reader.ReadLine())
                            )
                        );
                    }
                }
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }

            return vehicles;
        }
    }
}
