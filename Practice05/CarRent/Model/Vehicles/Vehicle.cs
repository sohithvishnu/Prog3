﻿using System;

namespace CarRent
{
    abstract class Vehicle : IComparable
    {
        //Enumerated list of possible vehicle statuses
        public enum Statuses
        {
            Available,
            Rented,
            InTransport
        }

        //Generic multiplier (coefficient) is stored in class property
        public static int DefaultFare { get; set; }  = 20000;

        //Automatic properties
        public string Licence { get; private set; }
        public int ProdYear { get; private set; }
        public string Id { get; private set; }

        public int Kms { get; set; }
        public Statuses Status { get; private set; }

        //Computed properties
        public bool IsAvailable
        {
            get
            {
                return Status == Statuses.Available;
            }
        }

        virtual public int Fare
        {
            get
            {
                return DefaultFare;
            }
        }

        public Vehicle(string licence, int prodYear, string id)
        {
            Licence = licence;
            ProdYear = prodYear;
            Id = id;
            Status = Statuses.Available;
        }

        #region Vehicle rent operations

        public bool Rent()
        {
            if(Status == Statuses.Available)
            {
                Status = Statuses.Rented;
                return true;
            }
            return false;
        }

        public bool Transport()
        {
            if (Status == Statuses.Available)
            {
                Status = Statuses.InTransport;
                return true;
            }
            return false;
        }

        public bool Return()
        {
            if (Status == Statuses.Available)
            {
                return false;
            }

            Status = Statuses.Available;
            return true;
        }

        #endregion

        public override string ToString()
        {
            //Enum value can be converted to string (Status is inserted into a string)
            return String.Format("Azonosító: {0}, rendszám: {1}, státusz: {2}", Id, Licence, Status);
        }

        #region IComparable interface implementation

        public int CompareTo(object obj)
        {
            if(obj is Vehicle)
            {
                return Licence.CompareTo((obj as Vehicle).Licence);
            }
            return -1;
        }

        #endregion
    }
}
