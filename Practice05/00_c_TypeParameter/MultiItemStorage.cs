﻿using System;

namespace _00_TypeParameter
{
    class MultiItemStorage<T>
    {
        private T[] items;

        public int Count { get; private set; }

        public int Size { get { return items.Length; } }

        public MultiItemStorage(int size)
        {
            items = new T[size];
            Count = 0;
        }

        public bool AddItem(T newItem)
        {
            if(Count < Size)
            {
                items[Count] = newItem;
                Count++;
                return true;
            }
            return false;
        }

        public T GetItem(int index)
        {
            return items[index];
        }

        public void Print(string caption)
        {
            Console.WriteLine(caption);
            for (int i = 0; i < Count; i++)
            {
                Console.WriteLine(items[i]);
            }
        }

        public void PrintAll(string caption)
        {
            Console.WriteLine(caption);
            foreach(T item in items)
            {
                Console.WriteLine(item);
            }
        }

        public bool RemoveItem(int index)
        {
            if (index < 0 || index >= items.Length)
                return false;

            for(int i=index; i<items.Length - 1;i++)
            {
                items[i] = items[i + 1];
            }

            Count = Count - 1;

            return true;
        }

        public void Sort(Func<T, T, int> comparer)
        {
            bool hasChange;

            do
            {
                hasChange = false;
                for (int i = 0; i < items.Length - 1; i++)
                {
                    if (comparer(items[i], items[i + 1]) > 0)
                    {
                        hasChange = true;
                        T temp = items[i];
                        items[i] = items[i + 1];
                        items[i + 1] = temp;
                    }
                }
            } while (hasChange);
        }

        public byte[] GetData()
        {
            //byte[] byteArray = new byte[items.Length * sizeof(T)];
            byte[] byteArray = new byte[items.Length * System.Runtime.InteropServices.Marshal.SizeOf(items[0])];
            Buffer.BlockCopy(items, 0, byteArray, 0, byteArray.Length);

            return byteArray;
        }
    }
}
