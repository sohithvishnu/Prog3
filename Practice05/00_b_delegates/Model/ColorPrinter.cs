﻿using System;

namespace _00_b_delegates
{
    class ColorPrinter : Printer
    {
        public string Color { get; set; }

        public ColorPrinter(string mode, string color) : base(mode)
        {
            Color = color;
        }

        public override void Print()
        {
            Console.WriteLine("Print in {0} color in {1} mode", Color, Mode);
        }
    }
}
