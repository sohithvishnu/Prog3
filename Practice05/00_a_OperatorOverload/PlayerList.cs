﻿using System;
using System.Collections.Generic;

namespace _00_a_OperatorOverload
{
    //Good use of properties and indexer
    //Interpretation is straitforward
    class PlayerList
    {
        private List<Player> players;

        public int Count { get { return players.Count; } }

        public Player this[int i]
        {
            get {
                return players[i];
            }
        }

        public Player this[string name]
        {
            get
            {
                foreach(Player player in players)
                {
                    if (player.Name.Equals(name)) return player;
                }
                throw new IndexOutOfRangeException(String.Format("No player found with the given name ({0})", name));
            }
        }

        public PlayerList()
        {
            players = new List<Player>();
        }

        public void AddPlayer(Player newPlayer)
        {
            players.Add(newPlayer);
        }
    }
}
