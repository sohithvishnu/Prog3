﻿using System;

namespace TempStat
{
    class Program
    {
        //Temperature statistics

        private const int DAY_COUNT = 30;

        private static Random random = new Random();

        static void Main(string[] args)
        {
            Stat stat = new Stat();

            Day newDay;
            for (int i = 0; i < DAY_COUNT; i++)
            {
                //newDay = GetDayFromConsole(stat.DayCount + 1);
                newDay = GetRandomDay();

                stat.AddDay(newDay);
            }

            stat.ListDays();

            Console.WriteLine("The average temperature of {0} continous days: {1}",
                stat.DayCount, stat.AvgTemp
            );

            //Selection of days with hottest and coolest average temperature.
            stat.Hottest();
            stat.Coldest();

            //******************************
            //  Selection of value list special points (min and max)
            //  using lambda as parameter, which specifies the item selection
            //******************************
            Console.WriteLine("A legmelegebb nap(ok):");

            //Selection of hottest days
            stat.Extremas((e, d) => e.AvgTemp < d.AvgTemp);

            Console.WriteLine("A leghidegebb nap(ok):");

            //Selection of coolest days
            stat.Extremas((e, d) => e.AvgTemp > d.AvgTemp);
            //******************************
        }

        private static Day GetDayFromConsole(int index)
        {
            Console.WriteLine("Day {0}", index);
            Console.Write("Temperature minimum: ");
            string minText = Console.ReadLine();
            Console.Write("Temperature maximum: ");
            string maxText = Console.ReadLine();

            return new Day(
                    Convert.ToInt32(minText),
                    Convert.ToInt32(maxText)
                );
        }

        private static Day GetRandomDay()
        {
            return new Day(
                    random.Next(19) - 9,
                    random.Next(24) - 2
                );
        }
    }
}
