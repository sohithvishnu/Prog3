﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using GitLogParser;
using GitLogParser.Model;

namespace GitLoggerSync
{
    public partial class MainForm : Form
    {
        GitData gitData = new GitData();

        public MainForm()
        {
            InitializeComponent();
        }

        private void getList_Click(object sender, EventArgs e)
        {
            Process cmd = new Process();
            cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = false;
            cmd.StartInfo.UseShellExecute = false;

            cmd.Start();

            cmd.StandardInput.WriteLine("dir");
            //cmd.StandardInput.WriteLine("git log --shortstat");
            cmd.StandardInput.Flush();

            cmd.StandardInput.WriteLine("exit");
            cmd.StandardInput.Flush();

            cmd.WaitForExit();

            string text = cmd.StandardOutput.ReadToEnd().Replace("\r\n", "</newline/>").Replace("\n", Environment.NewLine).Replace("</newline/>", Environment.NewLine);
            response.Text = text;

            Parser.ParseAll(
                response.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None), 
                gitData
            );
            for(int i=0; i<gitData.Count;i++)
            {
                log.Items.Add(gitData[i]);
            }
        }
    }
}
