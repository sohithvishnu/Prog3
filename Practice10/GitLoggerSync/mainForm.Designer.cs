﻿namespace GitLoggerSync
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.response = new System.Windows.Forms.TextBox();
            this.getList = new System.Windows.Forms.Button();
            this.log = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // response
            // 
            this.response.Location = new System.Drawing.Point(12, 12);
            this.response.Multiline = true;
            this.response.Name = "response";
            this.response.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.response.Size = new System.Drawing.Size(711, 625);
            this.response.TabIndex = 0;
            this.response.WordWrap = false;
            // 
            // getList
            // 
            this.getList.Location = new System.Drawing.Point(729, 14);
            this.getList.Name = "getList";
            this.getList.Size = new System.Drawing.Size(159, 73);
            this.getList.TabIndex = 1;
            this.getList.Text = "Get list sync";
            this.getList.UseVisualStyleBackColor = true;
            this.getList.Click += new System.EventHandler(this.getList_Click);
            // 
            // log
            // 
            this.log.FormattingEnabled = true;
            this.log.ItemHeight = 20;
            this.log.Location = new System.Drawing.Point(729, 93);
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(872, 544);
            this.log.TabIndex = 3;
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1613, 653);
            this.Controls.Add(this.log);
            this.Controls.Add(this.getList);
            this.Controls.Add(this.response);
            this.Name = "mainForm";
            this.Text = "GitLogger Sync";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox response;
        private System.Windows.Forms.Button getList;
        private System.Windows.Forms.ListBox log;
    }
}

