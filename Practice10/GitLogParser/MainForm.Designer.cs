﻿namespace GitLogParser
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fileContents = new System.Windows.Forms.TextBox();
            this.log = new System.Windows.Forms.ListBox();
            this.parse = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // fileContents
            // 
            this.fileContents.Location = new System.Drawing.Point(12, 12);
            this.fileContents.Multiline = true;
            this.fileContents.Name = "fileContents";
            this.fileContents.ReadOnly = true;
            this.fileContents.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.fileContents.Size = new System.Drawing.Size(991, 433);
            this.fileContents.TabIndex = 0;
            this.fileContents.TabStop = false;
            this.fileContents.WordWrap = false;
            // 
            // log
            // 
            this.log.FormattingEnabled = true;
            this.log.ItemHeight = 20;
            this.log.Location = new System.Drawing.Point(12, 460);
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(835, 504);
            this.log.TabIndex = 1;
            // 
            // parse
            // 
            this.parse.Location = new System.Drawing.Point(868, 460);
            this.parse.Name = "parse";
            this.parse.Size = new System.Drawing.Size(135, 56);
            this.parse.TabIndex = 2;
            this.parse.Text = "Parse";
            this.parse.UseVisualStyleBackColor = true;
            this.parse.Click += new System.EventHandler(this.parse_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1015, 990);
            this.Controls.Add(this.parse);
            this.Controls.Add(this.log);
            this.Controls.Add(this.fileContents);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "GitLogParser";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox fileContents;
        private System.Windows.Forms.ListBox log;
        private System.Windows.Forms.Button parse;
    }
}

