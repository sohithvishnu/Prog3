﻿using System;

namespace GitLogParser.Model
{
    public class Parser
    {
        private enum Flags
        {
            Id = 1,
            Author = 2,
            Date = 4,
            Stats = 8,
            All = 16
        }

        //Parse full log (all of its lines)
        //  Parsing state is managed in the class method
        public static void ParseAll(string[] lines, GitData gitData)
        {
            string id = "";
            string author = "";
            string date = "";
            string description = "";
            int fileCount = 0;
            int insert = 0;
            int delete = 0;
            byte found = 0;
            int number;

            foreach (string line in lines)
            {
                if(line.Trim().StartsWith("commit"))
                {
                    found |= (byte)Flags.Id;
                    id = line.Substring(7);
                }

                if (line.Trim().StartsWith("Author:"))
                {
                    found |= (byte)Flags.Author;
                    author = line.Substring(7);
                }

                if (line.Trim().StartsWith("Date:"))
                {
                    found |= (byte)Flags.Date;
                    date = line.Substring(7);
                    description = String.Empty;
                }

                if((found | (byte)Flags.Date) > 0 && (found | (byte)Flags.Stats) == 0)
                {
                    description += line;
                }

                if (line.Trim().Contains("files changed"))
                {
                    found |= (byte)Flags.Stats;
                    string[] parts = line.Split(',');
                    fileCount = insert = delete = 0;
                    for (int i = 0; i < parts.Length; i++)
                    {
                        bool success = int.TryParse(parts[i].Trim().Split(' ')[0], out number);

                        if (parts[i].Contains("files changed")) fileCount = number;
                        if (parts[i].Contains("insertions")) insert = number;
                        if (parts[i].Contains("deletions")) delete = number;
                    }

                    
                }

                if (found == (byte)Flags.All - 1)
                {
                    gitData.AddCommit(
                        new Commit(id, author, date, description, fileCount, insert, delete)
                    );
                    found = 0;
                }
            }
        }

        //Parse full log (all of its lines)
        //  Parsing state is managed by an object (instance of Parser)
        public static void ParseAllByLine(string[] lines, GitData gitData)
        {
            Parser parser = new Parser();

            foreach (string line in lines)
            {
                parser.Parse(line, gitData);
            }
        }

        #region Instance items

        //Parsing state instance representations
        private string id = "";
        private string author = "";
        private string date = "";
        private string description = "";
        private int fileCount = 0;
        private int insert = 0;
        private int delete = 0;
        private byte found = 0;
        private int number;

        //Instance method, using Parsing state instance representation declared above
        public void Parse(string line, GitData gitData)
        {
            if (line.Trim().StartsWith("commit"))
            {
                found |= (byte)Flags.Id;
                id = line.Substring(7);
            }

            if (line.Trim().StartsWith("Author:"))
            {
                found |= (byte)Flags.Author;
                author = line.Substring(7);
            }

            if (line.Trim().StartsWith("Date:"))
            {
                found |= (byte)Flags.Date;
                date = line.Substring(7);
                description = String.Empty;
            }

            if ((found | (byte)Flags.Date) > 0 && (found | (byte)Flags.Stats) == 0)
            {
                description += line;
            }

            if (line.Trim().Contains("files changed"))
            {
                found |= (byte)Flags.Stats;
                string[] parts = line.Split(',');
                fileCount = insert = delete = 0;
                for (int i = 0; i < parts.Length; i++)
                {
                    bool success = int.TryParse(parts[i].Trim().Split(' ')[0], out number);

                    if (parts[i].Contains("files changed")) fileCount = number;
                    if (parts[i].Contains("insertions")) insert = number;
                    if (parts[i].Contains("deletions")) delete = number;
                }


            }

            if (found == (byte)Flags.All - 1)
            {
                gitData.AddCommit(
                    new Commit(id, author, date, description, fileCount, insert, delete)
                );
                found = 0;
            }
        }

        #endregion
    }
}
