# Contents

# 0 - Swimming contest  
  
# 1 - Text to speech  
Crete a WindowsForms application, which uses Microsoft provided Text-to-Speech tools:
- add System.Speech reference
- add TrackBar control
- lists available voices
- gets a text message and reads it in a selected voice  
  
Add a TrackBar (Slider) Microsoft control.  

# 2 - Git log parser
The main aim of the project is to get list of git commits made on a specific repository.
From the received commits create business objects and then calculate statistics.  
Stats should include:
- list of users (e-mail) who made the commits
- average and variation of files modified
- average and variation of number of insertions
- average and variation of number of deletions
- average and variation of time interval lengths between commits  
  
## A. Parser components and their usage from file  
In the first version the git log is read from a file (created earlier indepent from the project).  
The project contains infrastructure to parse log and store commits.
The file parser process is wrapped into a WindowsForms application, which shows the contents of the file and also the list of stored commits.  

## B. Accessing git through Command Line (cmd.exe) synchronously
Modify Solution A. to get the log as the response of `git log` command executed in terminal (cmd.exe).
The command execution and answer reception is syschronous through StandardInput and StandardOutput of the process.  
Use classes declared in Solution A.  

## C. Make Command Line communication async
Modify Solution B. to get the response in an asychronous way.
Communication with terminal process is implemented by using Async calls and events (as callbacks).  

## D/1. Applying Single Responsibility principal 
Modify Solution C. to fulfill SOLID OOP principals, focusing on Single Responsibility.  
Move tasks of getting git log through terminal into an instance of a separate class.  
To communicate with the new object, use custom events and handlers.

## D/2. Calculate statistics of logged commits
*Extend* infrastructure of Solution D. with stat calculation tools, show the results on a separate Tab.  
