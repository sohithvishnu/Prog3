﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Uszoverseny {
    public partial class EredmenyForm : Form {

        private List<Versenyzo> versenyzok;

        public EredmenyForm(int tav, string versenyszam, List<Versenyzo> versenyzok) {
            InitializeComponent();

            this.versenyzok = versenyzok;

            lblCim.Text = tav.ToString() + " méteres " + versenyszam + "úszás eredményei:";

            versenyzok.Sort();
            ShowCompetitorList();
        }

        private void ShowCompetitorList()
        {
            foreach (Versenyzo v in versenyzok)
                lstVersenyzok.Items.Add(v);
        }

        private void lstVersenyzok_SelectedIndexChanged(object sender, EventArgs e)
        {
            Versenyzo versenyzo = (Versenyzo)lstVersenyzok.SelectedItem;
            txtRajtszam.Text = versenyzo.Nev;
            txtOrszag.Text = versenyzo.Orszag;
            txtIdoEredmeny.Text = versenyzo.IdoEredmeny.ToString();
        }

        private void btnBezar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void rdBtnNevsor_CheckedChanged(object sender, EventArgs e)
        {
            lstVersenyzok.Sorted = rdBtnNevsor.Checked;

            if(!lstVersenyzok.Sorted)
            {
                lstVersenyzok.Items.Clear();
                ShowCompetitorList();
            }
        }

        private void btnOrszagok_Click(object sender, EventArgs e)
        {
            OrszagokForm orszagokForm = new OrszagokForm(versenyzok);

            orszagokForm.ShowDialog();
        }
    }
}
