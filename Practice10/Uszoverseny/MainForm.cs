﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace Uszoverseny {
    public partial class MainForm : Form {

        private List<Versenyzo> versenyzok = new List<Versenyzo>();
        private int tav;
        private string versenyszam;

        public MainForm() {
            InitializeComponent();
            
            CenterToScreen();
            
            openFileDialog1.InitialDirectory = Environment.CurrentDirectory;
            openFileDialog1.Title = "Versenyzők betöltése";
            openFileDialog1.FileName = "versenyzok.txt";

            saveFileDialog1.InitialDirectory = Environment.CurrentDirectory;
            saveFileDialog1.FileName = "eredmeny.txt";
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e) {
            versenyToolStripMenuItem.Enabled = AdatBevitel();
        }

        private bool AdatBevitel() {
            bool retVal = false;
            if (openFileDialog1.ShowDialog() == DialogResult.OK) {
                StreamReader streamReader = null;
                try
                {
                    streamReader = new StreamReader(openFileDialog1.FileName);
                    string[] words;
                    while (!streamReader.EndOfStream)
                    {
                        words = streamReader.ReadLine().Split(';');
                        if (words.Length >= 3)
                        {
                            versenyzok.Add(
                                new Versenyzo(words[0], words[1], words[2])
                            );
                        }
                    }
                    retVal = true;
                }
                catch(Exception ex)
                {
                    MessageBox.Show(
                        "Fájl olvasási hiba!" + Environment.NewLine + ex.Message,
                        "Hiba"
                    );
                }
                finally
                {
                    if(streamReader != null)streamReader.Close();
                }
            }

            return retVal;
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e) {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK) {
                StreamWriter writer = null;
                try
                {
                    writer = new StreamWriter(saveFileDialog1.FileName);
                    foreach (Versenyzo v in versenyzok)
                    {
                        writer.WriteLine(v.GetContentString());
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        "Fájl írási hiba!" + Environment.NewLine + ex.Message,
                        "Hiba"
                    );
                }
                finally
                {
                    if(writer != null)writer.Close();
                }
            }
        }

        private void versenyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            VersenyForm versenyForm = new VersenyForm(versenyzok);

            Hide();
            versenyForm.ShowDialog();
            tav = versenyForm.Tav;
            versenyszam = versenyForm.Versenyszam;
            Show();

            eredményToolStripMenuItem.Enabled = true;
        }

        private void eredményToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EredmenyForm eredmenyForm = new EredmenyForm(
                tav,
                versenyszam,
                versenyzok
            );

            Hide();
            eredmenyForm.ShowDialog();
            Show();
        }

        private void névjegyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Prog.3, Gyak.09, 1. feladat (2020)", "Névjegy");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
