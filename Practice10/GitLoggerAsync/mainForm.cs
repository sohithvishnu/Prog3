﻿using GitLogParser;
using GitLogParser.Model;
using System;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;

namespace GitLoggerAsync
{
    public partial class MainForm : Form
    {
        Process cmd;
        bool parseOutput = false;

        GitData gitData = new GitData();
        Parser gitLogParser = new Parser();

        public MainForm()
        {
            InitializeComponent();
            ((Control)gitPage).Enabled = false;
        }

        private void Cmd_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            string dataLine = string.Format("{0}{1}", e.Data, Environment.NewLine);
            textBox1.Invoke((MethodInvoker)delegate { textBox1.Text += dataLine; });

            if (dataLine.Contains("ListEnd"))
            {
                parseOutput = false;
                commits.Items.Clear();
                for (int i = 0; i < gitData.Count; i++)
                {
                    commits.Items.Add(gitData[i]);
                }

                terminalPage.Invoke((MethodInvoker)delegate { terminalPage.Enabled = true; });
            }

            if (parseOutput)
            {
                gitLogParser.Parse(dataLine, gitData);
            }
        }

        private void Cmd_Exited(object sender, EventArgs e)
        {
            textBox1.Invoke((MethodInvoker)delegate { textBox1.Text += string.Format("Process closed{0}", Environment.NewLine); });
        }

        private void init_Click(object sender, EventArgs e)
        {
            cmd = new Process();
            cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.StandardOutputEncoding = Encoding.UTF8;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;

            cmd.EnableRaisingEvents = true;
            cmd.OutputDataReceived += Cmd_OutputDataReceived;
            cmd.Exited += Cmd_Exited;

            cmd.Start();

            cmd.BeginOutputReadLine();

            initProcess.Enabled = false;
            listCommits.Enabled = true;
            closeProcess.Enabled = true;
            ((Control)gitPage).Enabled = true;
        }

        private void log_Click(object sender, EventArgs e)
        {
            terminalPage.Enabled = false;
            parseOutput = true;

            cmd.StandardInput.WriteLine("git log --shortstat");
            cmd.StandardInput.Flush();
            cmd.StandardInput.WriteLine("echo ListEnd");
            cmd.StandardInput.Flush();
        }

        private void close_Click(object sender, EventArgs e)
        {
            cmd.StandardInput.WriteLine("exit");
            cmd.StandardInput.Flush();

            initProcess.Enabled = true;
            listCommits.Enabled = false;
            closeProcess.Enabled = false;
            ((Control)gitPage).Enabled = false;
        }
    }
}
