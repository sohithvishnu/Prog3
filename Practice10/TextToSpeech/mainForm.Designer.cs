﻿namespace TextToSpeech
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.speak = new System.Windows.Forms.Button();
            this.toSpeak = new System.Windows.Forms.TextBox();
            this.availableVoices = new System.Windows.Forms.ListBox();
            this.volume = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.volume)).BeginInit();
            this.SuspendLayout();
            // 
            // speak
            // 
            this.speak.Enabled = false;
            this.speak.Location = new System.Drawing.Point(742, 244);
            this.speak.Name = "speak";
            this.speak.Size = new System.Drawing.Size(75, 37);
            this.speak.TabIndex = 0;
            this.speak.Text = "Speak";
            this.speak.UseVisualStyleBackColor = true;
            this.speak.Click += new System.EventHandler(this.speak_Click);
            // 
            // toSpeak
            // 
            this.toSpeak.Location = new System.Drawing.Point(12, 244);
            this.toSpeak.Multiline = true;
            this.toSpeak.Name = "toSpeak";
            this.toSpeak.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.toSpeak.Size = new System.Drawing.Size(724, 115);
            this.toSpeak.TabIndex = 2;
            this.toSpeak.Text = "Please enter some text to be spoken!\r\nThe text could contain multiple lines.\r\nSee" +
    " what is happening in that case!";
            // 
            // availableVoices
            // 
            this.availableVoices.FormattingEnabled = true;
            this.availableVoices.ItemHeight = 16;
            this.availableVoices.Location = new System.Drawing.Point(12, 12);
            this.availableVoices.Name = "availableVoices";
            this.availableVoices.Size = new System.Drawing.Size(805, 164);
            this.availableVoices.TabIndex = 3;
            this.availableVoices.SelectedIndexChanged += new System.EventHandler(this.availableVoices_SelectedIndexChanged);
            // 
            // volume
            // 
            this.volume.Location = new System.Drawing.Point(12, 182);
            this.volume.Maximum = 100;
            this.volume.Name = "volume";
            this.volume.Size = new System.Drawing.Size(805, 56);
            this.volume.TabIndex = 4;
            this.volume.Tag = "";
            this.volume.Value = 75;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(387, 221);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Volume";
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 371);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.volume);
            this.Controls.Add(this.availableVoices);
            this.Controls.Add(this.toSpeak);
            this.Controls.Add(this.speak);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.volume)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button speak;
        private System.Windows.Forms.TextBox toSpeak;
        private System.Windows.Forms.ListBox availableVoices;
        private System.Windows.Forms.TrackBar volume;
        private System.Windows.Forms.Label label1;
    }
}

