﻿namespace GitLoggerAsyncEvt
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listCommits = new System.Windows.Forms.Button();
            this.responseText = new System.Windows.Forms.TextBox();
            this.closeProcess = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.terminalPage = new System.Windows.Forms.TabPage();
            this.gitPage = new System.Windows.Forms.TabPage();
            this.commitDetails = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.commits = new System.Windows.Forms.ListBox();
            this.statPage = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.terminalPage.SuspendLayout();
            this.gitPage.SuspendLayout();
            this.SuspendLayout();
            // 
            // listCommits
            // 
            this.listCommits.Location = new System.Drawing.Point(496, 5);
            this.listCommits.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.listCommits.Name = "listCommits";
            this.listCommits.Size = new System.Drawing.Size(84, 41);
            this.listCommits.TabIndex = 0;
            this.listCommits.Text = "List";
            this.listCommits.UseVisualStyleBackColor = true;
            this.listCommits.Click += new System.EventHandler(this.log_Click);
            // 
            // responseText
            // 
            this.responseText.Location = new System.Drawing.Point(5, 5);
            this.responseText.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.responseText.Multiline = true;
            this.responseText.Name = "responseText";
            this.responseText.ReadOnly = true;
            this.responseText.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.responseText.Size = new System.Drawing.Size(486, 307);
            this.responseText.TabIndex = 1;
            this.responseText.WordWrap = false;
            // 
            // closeProcess
            // 
            this.closeProcess.Location = new System.Drawing.Point(496, 270);
            this.closeProcess.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.closeProcess.Name = "closeProcess";
            this.closeProcess.Size = new System.Drawing.Size(84, 41);
            this.closeProcess.TabIndex = 3;
            this.closeProcess.Text = "Close";
            this.closeProcess.UseVisualStyleBackColor = true;
            this.closeProcess.Click += new System.EventHandler(this.close_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.terminalPage);
            this.tabControl1.Controls.Add(this.gitPage);
            this.tabControl1.Controls.Add(this.statPage);
            this.tabControl1.Location = new System.Drawing.Point(11, 10);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(596, 358);
            this.tabControl1.TabIndex = 5;
            // 
            // terminalPage
            // 
            this.terminalPage.Controls.Add(this.responseText);
            this.terminalPage.Controls.Add(this.listCommits);
            this.terminalPage.Controls.Add(this.closeProcess);
            this.terminalPage.Location = new System.Drawing.Point(4, 25);
            this.terminalPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.terminalPage.Name = "terminalPage";
            this.terminalPage.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.terminalPage.Size = new System.Drawing.Size(588, 329);
            this.terminalPage.TabIndex = 0;
            this.terminalPage.Text = "Terminal";
            this.terminalPage.UseVisualStyleBackColor = true;
            // 
            // gitPage
            // 
            this.gitPage.Controls.Add(this.commitDetails);
            this.gitPage.Controls.Add(this.label1);
            this.gitPage.Controls.Add(this.commits);
            this.gitPage.Location = new System.Drawing.Point(4, 25);
            this.gitPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gitPage.Name = "gitPage";
            this.gitPage.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gitPage.Size = new System.Drawing.Size(588, 329);
            this.gitPage.TabIndex = 1;
            this.gitPage.Text = "Git";
            this.gitPage.UseVisualStyleBackColor = true;
            // 
            // commitDetails
            // 
            this.commitDetails.Location = new System.Drawing.Point(9, 198);
            this.commitDetails.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.commitDetails.Multiline = true;
            this.commitDetails.Name = "commitDetails";
            this.commitDetails.ReadOnly = true;
            this.commitDetails.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.commitDetails.Size = new System.Drawing.Size(568, 116);
            this.commitDetails.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Commits";
            // 
            // commits
            // 
            this.commits.FormattingEnabled = true;
            this.commits.ItemHeight = 16;
            this.commits.Location = new System.Drawing.Point(9, 30);
            this.commits.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.commits.Name = "commits";
            this.commits.Size = new System.Drawing.Size(568, 164);
            this.commits.TabIndex = 5;
            // 
            // statPage
            // 
            this.statPage.Location = new System.Drawing.Point(4, 25);
            this.statPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.statPage.Name = "statPage";
            this.statPage.Size = new System.Drawing.Size(588, 329);
            this.statPage.TabIndex = 2;
            this.statPage.Text = "Stats";
            this.statPage.UseVisualStyleBackColor = true;
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(625, 388);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Git Log Analyzer v2.0";
            this.tabControl1.ResumeLayout(false);
            this.terminalPage.ResumeLayout(false);
            this.terminalPage.PerformLayout();
            this.gitPage.ResumeLayout(false);
            this.gitPage.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button listCommits;
        private System.Windows.Forms.TextBox responseText;
        private System.Windows.Forms.Button closeProcess;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage terminalPage;
        private System.Windows.Forms.TabPage gitPage;
        private System.Windows.Forms.TextBox commitDetails;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox commits;
        private System.Windows.Forms.TabPage statPage;
    }
}

