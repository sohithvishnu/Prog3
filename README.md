# Prog3
PTE MIK Programming 3: solution of practical examples

A VisualStudio 2017 Community solutions contain:  
- Contents of practice lectures in separate projects: `PracticeXX` - contents of practice class XX

After understanding and learning contents of theory lectures, try to solve the practical task on your own.  
When finished, compare solution to the presented.  
If they match, you can apply the knowledge of the lecture.  
If they do not match, try to find out how the example works and why made that way.  
If you could get the idea, then update your solution based on new point of view.
If you could not get the idea, **ASK THE LECTURER**!
