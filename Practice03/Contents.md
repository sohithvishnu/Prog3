# Contents of Practice 03
In this lecture you see how to:
- initialize objects by constructors
- use fix size and dynamic size storages: `array` and `List`
- load and parse text files
- format data to string
- handle exceptions
  
## 00 - Initialization by constructor
Create an application to sotre instances of rentable estates.  
Estates could have garden and garage. Both properties are represented by boolean data members.  
Try to desing the initialization of instances.  
If no value is specified for a property, its default value is `false`.

How to:
- declare parameters with default values to get rid of method signature match
- call other constructor to outsource already implemented part of initialization
- initialize property out-of but with the constructor
  
## 03 - Singing competition  
Create an application which:
- loads competitors from a text file.  
  In `Competition.LoadCompetitors(...)` IOExceptions could occure, but the method and even the class does not handle them. This is not visible in the class or method declaration.  
  **In C# there are no checked exceptions!**  
- Competitor class
  - Constructor is overridden if developer wants to provide Id or let the class use the sequence number of instance
  - overriding constructor uses default Id value (from class state) and initialization implemented in first constructor
  - static constructor initializes static data member `nextId`
- simulate that the jury evaluates all the productions (by random number generation).  
- show how basic array algorithms work in C#
  - find the winner (maximum)
  - list all the winner (selection)
  - sort by points or Id (sort)
  - find a competitor by Id (search)
  
The difference between this solution and Practice02/02 is that this application stores all competitors and operates on this collection, while AnimalContest works with only two (current and winner).  
  
`CompetitionWithArray` class stores competitors in a fixed length array.  
`Sort` and `Find` operations are implemented in static method of `Array` class. Predicates have to check if item is null!  
When more competitors are in the source file than the size or the storage, a custom exception is thrown and handled (`TooManyCompetitorsException`). Its body is empty, because its existence describes the concrete error.
  
`Competition` class stores competitors in a dynamic length generic `List`.  
`Sort` and `Find` operations are implemented as instance methods of the List class.
  
Try to modify the solution to conform **single responsibility pricipal**. (Focus on file handling.)
  
## 07 - Temperature statistics
As in Practice 03/07, create an application to store minimum and maximum temperature of days. From this, average temperature can be calculated.  
Store entered days, select coolest and hottest days and list average temperature of continous days.
  