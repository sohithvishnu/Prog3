﻿using SingingCompetition.Model;
using System;
using System.IO;

namespace SingingCompetition
{
    class Program
    {
        private const int JURY_COUNT = 3;
        private const int POINTS_MIN = 1;
        private const int POINTS_MAX = 5;

        private static CompetitionWithArray competition;

        static void Main(string[] args)
        {
            competition = new CompetitionWithArray(JURY_COUNT, POINTS_MIN, POINTS_MAX, 10);

            try
            {
                competition.Start("..\\..\\versenyzok.txt");
                FindCompetitors();
            }catch(FileNotFoundException)
            {
                Console.WriteLine("File could not be found!");
            }
            catch (IOException ex)
            {
                Console.WriteLine("File read error! {0}", ex.Message);
            }
            catch (TooManyCompetitorsException)
            {
                Console.WriteLine("Too many competitors in the file!");
            }
        }

        private static void FindCompetitors()
        {
            int id;
            do
            {
                id = Input.GetNumerFromConsole("A keresett rajtszám (kilépés = -1): ");
                if (id > 0) {
                    FindCompetitor(id);
                }
            }
            while (id != -1);
        }

        static void FindCompetitor(int id)
        {
            Competitor found = competition.Find(id);

            if(found == null) {
                Console.WriteLine("Sajnos nincs ilyen versenyző!");
            }
            else {
                Console.WriteLine("A talált versenyző: {0}", found);
            }
        }
    }
}
