﻿using System;
using System.IO;

namespace SingingCompetition.Model
{
    class CompetitionWithArray
    {
        //Read-only instance properties to let use different settings for each competitions
        public int JuryCount { get; private set; }
        public int PointsMin { get; private set; }
        public int PointsMax { get; private set; }

        //Private data members
        private Competitor[] competitors;
        private int competitorCount;
        private Random random;

        public CompetitionWithArray(int juryCount, int pointsMin, int pointsmax, int maxCompetitorCount)
        {
            JuryCount = juryCount;
            PointsMin = pointsMin;
            PointsMax = pointsmax;

            competitors = new Competitor[maxCompetitorCount];
            competitorCount = 0;
            random = new Random();
        }

        public void Start(string fileName)
        {
            LoadCompetitors(fileName);
            ListCompetitors();

            Input.Pause();

            EvaluateCompetitors();

            Input.Pause();

            Console.WriteLine("---=== Points after evaluation ===---");
            ListCompetitors();

            Input.Pause();

            Console.WriteLine("A győztes: {0}", GetOneWinner());

            Input.Pause();

            Console.WriteLine("---=== All winners ===---");
            ListWinnersBySort();

            Input.Pause();

            Console.WriteLine("---=== Order by points ===---");
            SortByPoints();
            ListCompetitors();

            Input.Pause();

            Console.WriteLine("---=== Order by ID ===---");
            SortById();
            ListCompetitors();

            Input.Pause();
        }

        public void LoadCompetitors(string fileName)
        {
            StreamReader reader = new StreamReader(fileName);

            while (!reader.EndOfStream)
            {
                try
                {
                    competitors[competitorCount++] =
                        new Competitor(
                            //competitors.Count + 1,        //this parameter has default value, so it does not have to be provided (but can be)
                            reader.ReadLine(),
                            reader.ReadLine()
                        );
                }
                //Convert the generic exception to a very specific one
                catch(IndexOutOfRangeException)
                {
                    throw new TooManyCompetitorsException();
                }
            }

            reader.Close();
        }

        public void ListCompetitors()
        {
            for (int i = 0; i < competitorCount; i++)
            {
                Console.WriteLine(competitors[i]);
            }
        }

        public void EvaluateCompetitors()
        {
            int newPoints;
            for (int i = 0; i < competitorCount; i++)
            {
                for (int jury = 1; jury <= JuryCount; jury++)
                {
                    newPoints = random.Next(PointsMin, PointsMax + 1);
                    Console.WriteLine(
                        "A {0} sorszámú versenyző az {1}. zsűritagtól {2} pontot kapott",
                        competitors[i].Id, jury, newPoints
                    );

                    //Use the property setter with validation
                    //  the custom behaviour of setter is not obvious
                    //competitor.Points += newPoints;
                    //
                    //Therefor use custom method to implement specific, named behaviour
                    competitors[i].AddPoints(newPoints);
                }
            }
        }

        //Get the first competitor who has the most points
        public Competitor GetOneWinner()
        {
            Competitor winner = competitors[0];
            for (int i = 0; i < competitorCount; i++)
            {
                if(winner.Points < competitors[i].Points)
                {
                    winner = competitors[i];
                }
            }

            return winner;
        }
        
        public void SortByPoints()
        {
            //Direction of comparer is inverted by '-' (negative sign)
            //So most point is the first
            Array.Sort(competitors, (x, y) => x == null ? 1 : (y == null ? -1 : -x.Points.CompareTo(y.Points)));
        }

        //Get the most point and list all competitors who has it (tie)
        //The loop checks all competitors
        public void ListWinnersBySearch()
        {
            Competitor winner = GetOneWinner();
            Console.WriteLine("A győztesek:");
            for (int i = 0; i < competitorCount; i++)
            {
                if (winner.Points == competitors[i].Points)
                {
                    Console.WriteLine("\t{0}", competitors[i]);
                }
            }
        }

        //Sort, then list all who has same points as winner
        //The loop exits over tie
        public void ListWinnersBySort()
        {
            SortByPoints();

            Competitor winner = competitors[0];
            int idx = 0;
            Console.WriteLine("A győztesek:");
            while (idx < competitorCount && competitors[idx].Points == winner.Points)
            {
                Console.WriteLine("\t{0}", competitors[idx++]);
            }
        }

        public void SortById()
        {
            Array.Sort(competitors, (x, y) => x== null ? 1 : (y==null ? -1 : x.Id.CompareTo(y.Id)));
        }

        public Competitor Find(int id)
        {
            SortById();

            return Array.Find(competitors, x => x == null ? false : x.Id == id);
        }
    }
}
