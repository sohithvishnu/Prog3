﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SingingCompetition.Model
{
    class Competition
    {
        //Read-only instance properties to let use different settings for each competitions
        public int JuryCount { get; private set; }
        public int PointsMin { get; private set; }
        public int PointsMax { get; private set; }

        //Private data members
        private List<Competitor> competitors;
        private Random random;

        public Competition(int juryCount, int pointsMin, int pointsmax)
        {
            JuryCount = juryCount;
            PointsMin = pointsMin;
            PointsMax = pointsmax;

            competitors = new List<Competitor>();
            random = new Random();
        }

        public void Start(string fileName)
        {
            LoadCompetitors(fileName);
            ListCompetitors();

            Input.Pause();

            EvaluateCompetitors();

            Input.Pause();

            Console.WriteLine("---=== Points after evaluation ===---");
            ListCompetitors();

            Input.Pause();

            Console.WriteLine("A győztes: {0}", GetOneWinner());

            Input.Pause();

            Console.WriteLine("---=== All winners ===---");
            ListWinnersBySort();

            Input.Pause();

            Console.WriteLine("---=== Order by points ===---");
            SortByPoints();
            ListCompetitors();

            Input.Pause();

            Console.WriteLine("---=== Order by ID ===---");
            SortById();
            ListCompetitors();

            Input.Pause();
        }

        public void LoadCompetitors(string fileName)
        {
            StreamReader reader = new StreamReader(fileName);

            while (!reader.EndOfStream)
            {
                competitors.Add(
                    new Competitor(
                        //competitors.Count + 1,        //this parameter has default value, so it does not have to be provided (but can be)
                        reader.ReadLine(),
                        reader.ReadLine()
                    )
                );
            }

            reader.Close();
        }

        public void ListCompetitors()
        {
            foreach(Competitor competitor in competitors)
            {
                Console.WriteLine(competitor);
            }
        }

        public void EvaluateCompetitors()
        {
            int newPoints;
            foreach (Competitor competitor in competitors)
            {
                for (int jury = 1; jury <= JuryCount; jury++)
                {
                    newPoints = random.Next(PointsMin, PointsMax + 1);
                    Console.WriteLine(
                        "A {0} sorszámú versenyző az {1}. zsűritagtól {2} pontot kapott",
                        competitor.Id, jury, newPoints
                    );

                    //Use the property setter with validation
                    //  the custom behaviour of setter is not obvious
                    //competitor.Points += newPoints;
                    //
                    //Therefor use custom method to implement specific, named behaviour
                    competitor.AddPoints(newPoints);
                }
            }
        }

        //Get the first competitor who has the most points
        public Competitor GetOneWinner()
        {
            Competitor winner = competitors[0];
            foreach(Competitor competitor in competitors)
            {
                if(winner.Points < competitor.Points)
                {
                    winner = competitor;
                }
            }

            return winner;
        }
        
        public void SortByPoints()
        {
            //Direction of comparer is inverted by '-' (negative sign)
            //So most point is the first
            competitors.Sort((x, y) => -x.Points.CompareTo(y.Points));
        }

        //Get the most point and list all competitors who has it (tie)
        //The loop checks all competitors
        public void ListWinnersBySearch()
        {
            Competitor winner = GetOneWinner();
            Console.WriteLine("A győztesek:");
            foreach(var competitor in competitors)
            {
                if (winner.Points == competitor.Points)
                {
                    Console.WriteLine("\t{0}", competitor);
                }
            }
        }

        //Sort, then list all who has same points as winner
        //The loop exits over tie
        public void ListWinnersBySort()
        {
            SortByPoints();

            Competitor winner = competitors[0];
            int idx = 0;
            Console.WriteLine("A győztesek:");
            while (idx < competitors.Count && competitors[idx].Points == winner.Points)
            {
                Console.WriteLine("\t{0}", competitors[idx++]);
            }
        }

        public void SortById()
        {
            competitors.Sort((x, y) => x.Id.CompareTo(y.Id));
        }

        public Competitor Find(int id)
        {
            SortById();

            return competitors.Find(x => x.Id == id);
        }
    }
}
