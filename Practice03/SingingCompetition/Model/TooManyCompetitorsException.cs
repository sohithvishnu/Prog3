﻿using System;

namespace SingingCompetition.Model
{
    //The body is empty, because the occurrence of the exception clearly describes the error.
    class TooManyCompetitorsException : Exception
    {

    }
}
