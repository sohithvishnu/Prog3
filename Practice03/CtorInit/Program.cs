﻿using System;

namespace CtorInit
{
    class Program
    {
        static void Main(string[] args)
        {
            Estate flat1 = new Estate("flat", 3)
            {
                ShopNearby = true
            };

            Estate house1 = new Estate("house", 5, garage: true);
            Estate cottage1 = new Estate("summer house", 2, garden: true)
            {
                ShopNearby = true
            };

            Estate house2 = new Estate("house", 6, garden: true, garage: true);
            Estate house3 = new Estate("house", 7, true, true)
            {
                ShopNearby = true
            };

            Estate cottage2 = new Estate("summer house", 5, true);

            Console.WriteLine(flat1);
            Console.WriteLine(house1);
            Console.WriteLine(cottage1);
            Console.WriteLine(house2);
            Console.WriteLine(house3);
        }
    }
}
