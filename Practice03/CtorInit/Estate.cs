﻿namespace CtorInit
{
    class Estate
    { 
        public string Type { get; private set; }
        public int Rooms { get; private set; }
        public bool HasGarden { get; private set; }
        public bool HasGarage { get; private set; }
        public bool ShopNearby { get; set; }

        public Estate(string type, int rooms, bool garden = false, bool garage = false)
        {
            Type = type;
            Rooms = rooms;
            HasGarden = garden;
            HasGarage = garage;
        }

        public Estate(string type, int rooms, bool shopNearby) : this(type, rooms)
        {
            ShopNearby = shopNearby;
        }

        public override string ToString()
        {
            return string.Format("This {0} has {1} rooms {2} garden, {3} garage", 
                Type, Rooms,
                HasGarden ? "with" : "without",
                HasGarage ? "with" : "without"
            );
        }
    }
}
