﻿using System;
using System.Collections.Generic;

namespace TempStat
{
    class Stat
    {
        List<Day> days;
        int sumOfAvgs;

        public int DayCount
        {
            get { return days.Count; }
        }

        public int AvgTemp
        {
            get { return sumOfAvgs / days.Count; }
        }

        public Stat()
        {
            days = new List<Day>();
            sumOfAvgs = 0;
        }

        public void AddDay(Day newDay)
        {
            days.Add(newDay);
            sumOfAvgs += newDay.AvgTemp;
        }

        public void ListDays()
        {
            foreach(Day day in days)
            {
                Console.WriteLine(day);
            }
        }

        public void Hottest()
        {
            Day hot = null;
            foreach(Day day in days)
            {
                if(hot == null || hot.AvgTemp < day.AvgTemp)
                {
                    hot = day;
                }
            }

            Console.WriteLine("Hottest day(s):");
            foreach (Day day in days)
            {
                if (hot.AvgTemp == day.AvgTemp)
                {
                    Console.WriteLine(day);
                }
            }
        }

        public void Coldest()
        {
            Day cold = null;
            foreach (Day day in days)
            {
                if (cold == null || cold.AvgTemp > day.AvgTemp)
                {
                    cold = day;
                }
            }

            Console.WriteLine("Coolest day(s):");
            foreach (Day day in days)
            {
                if (cold.AvgTemp == day.AvgTemp)
                {
                    Console.WriteLine(day);
                }
            }
        }
    }
}
