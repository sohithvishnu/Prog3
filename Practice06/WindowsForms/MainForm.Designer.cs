﻿namespace TextWindowsForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lastName = new System.Windows.Forms.TextBox();
            this.firstName = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.phoneNumber = new System.Windows.Forms.MaskedTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.greetings = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            this.message = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vezetéknév:";
            // 
            // lastName
            // 
            this.lastName.Location = new System.Drawing.Point(159, 22);
            this.lastName.Name = "lastName";
            this.lastName.Size = new System.Drawing.Size(197, 22);
            this.lastName.TabIndex = 1;
            // 
            // firstName
            // 
            this.firstName.Location = new System.Drawing.Point(159, 50);
            this.firstName.Name = "firstName";
            this.firstName.Size = new System.Drawing.Size(197, 22);
            this.firstName.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(60, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Keresztnév";
            // 
            // phoneNumber
            // 
            this.phoneNumber.Location = new System.Drawing.Point(159, 78);
            this.phoneNumber.Mask = "(00) 000-0000";
            this.phoneNumber.Name = "phoneNumber";
            this.phoneNumber.Size = new System.Drawing.Size(197, 22);
            this.phoneNumber.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(60, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Telefonszám:";
            // 
            // greetings
            // 
            this.greetings.Location = new System.Drawing.Point(63, 221);
            this.greetings.Name = "greetings";
            this.greetings.Size = new System.Drawing.Size(105, 26);
            this.greetings.TabIndex = 6;
            this.greetings.Text = "Greetings";
            this.greetings.UseVisualStyleBackColor = true;
            this.greetings.Click += new System.EventHandler(this.greetings_Click);
            // 
            // exit
            // 
            this.exit.Location = new System.Drawing.Point(251, 221);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(105, 26);
            this.exit.TabIndex = 7;
            this.exit.Text = "Exit";
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // message
            // 
            this.message.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.message.Location = new System.Drawing.Point(62, 106);
            this.message.Multiline = true;
            this.message.Name = "message";
            this.message.ReadOnly = true;
            this.message.Size = new System.Drawing.Size(293, 109);
            this.message.TabIndex = 8;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(427, 272);
            this.Controls.Add(this.message);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.greetings);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.phoneNumber);
            this.Controls.Add(this.firstName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lastName);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "First WindowsForms application";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox lastName;
        private System.Windows.Forms.TextBox firstName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MaskedTextBox phoneNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button greetings;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.TextBox message;
    }
}

