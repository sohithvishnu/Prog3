﻿using System;

namespace Events
{
    class Program
    {
        static void Main(string[] args)
        {
            HandleKeyPress();

            //CreateInterruptedEventLoop();

            //CreateContinousWiredEventLoop();

            //CreateEventLoopWithSingleHandlers();

            //CreateEventLoopWithMultipleHandlers();

            //CreateEventLoopWithDynamicMultipleHandlers();

            //CreateGenericEventHandler();

            //CreateMulticastEventHandler();

            //CreateRealEventHandler();
        }

        static void HandleKeyPress()
        {
            ConsoleKeyInfo key = Console.ReadKey(false);
            while (char.ToLower(key.KeyChar) != 'q')
            {
                //Do the job
                Console.Write(".");

                key = Console.ReadKey(false);
            }
        }

        static void CreateInterruptedEventLoop()
        {
            //Create event loop, inject in-line created event handler
            InterruptedEventLoop interrupted = new InterruptedEventLoop(new WiredEventHandler());

            //Start event loop
            StartEventLoop("intercepted", "Exit the loop by pressing 'q'", interrupted);
        }

        static void CreateContinousWiredEventLoop()
        {
            //A continous loop to read single keys checking if key has been pressed,
            //    if yes, try to handle the event
            //    if no, new iteration is done
            // Event handling is wired into the event loop

            //Create event loop, inject in-line created event handler
            ContinousEventLoop continous = new ContinousEventLoop(new WiredEventHandler());

            //Start event loop
            StartEventLoop("continous wired", "Exit the loop by pressing 'q'", continous);
        }

        static void CreateEventLoopWithSingleHandlers()
        {
            //Create event handler collection
            SingleEventHandlers singleHandlers = new SingleEventHandlers();

            //Create event loop, inject handlers
            ContinousEventLoop singles = new ContinousEventLoop(singleHandlers);

            //Add event handlers for specific KeyPress events
            singleHandlers.SetEventHandler('b', () => { Console.Write(".oOo."); return true; });
            singleHandlers.SetEventHandler('q', () => false);
            singleHandlers.SetEventHandler('x', () => false);

            //Start event loop
            StartEventLoop("continous dynamic", "Try 'b', exit the loop by pressing 'q' or 'x'", singles);
        }

        static void CreateEventLoopWithMultipleHandlers()
        {
            //Create event handler collection
            MultiEventHandlers multiHandlers = new MultiEventHandlers();

            //Create event loop, inject handlers
            ContinousEventLoop multiples = new ContinousEventLoop(multiHandlers);

            //Add event handlers for specific KeyPress events
            multiHandlers.AddSpecificEventHandler('b', PrintOs);
            multiHandlers.AddSpecificEventHandler('x', () => { Console.Write(".oOX"); return true; });
            multiHandlers.AddSpecificEventHandler('q', () => false);

            //Add multiple handlers for 'x' keypress event
            multiHandlers.AddSpecificEventHandler('x', () => false);

            //Start event loop
            StartEventLoop("continous multihandled", "Try 'b', exit the loop by pressing 'q' or 'x'->.oOX", multiples);
        }

        static void CreateEventLoopWithDynamicMultipleHandlers()
        {
            //This example uses the same event handler collection as the previous, 
            //but exteds handlers with dynamic methods, which modify the event handler collection itself

            //Create event handler collection
            MultiEventHandlers dynamicHandlers = new MultiEventHandlers();

            //Create event loop, inject handlers
            ContinousEventLoop dynamic = new ContinousEventLoop(dynamicHandlers);

            //Add event handlers for specific KeyPress events
            dynamicHandlers.AddSpecificEventHandler('b', PrintOs);
            dynamicHandlers.AddSpecificEventHandler('x', () => { Console.Write(".oOX"); return true; });
            dynamicHandlers.AddSpecificEventHandler('q', () => false);

            //Add multiple handlers for 'x' keypress event
            dynamicHandlers.AddSpecificEventHandler('x', () => false);

            //Add dynamic event handlers
            //!!!!!!
            //Note that: same handler can be added multiple times
            //!!!!!!
            dynamicHandlers.AddSpecificEventHandler('a', () =>
            {
                dynamicHandlers.AddSpecificEventHandler('b', PrintOs);
                Console.Write(dynamicHandlers.GetHandlerCount('b'));
                return true;
            });

            //Remove (first occurrence of) previously added handler
            //!!!!!!
            //Note that: same handler can be removed multiple times
            //!!!!!!
            dynamicHandlers.AddSpecificEventHandler('r', () =>
            {
                dynamicHandlers.RemoveSpecificEventHandler('b', PrintOs);
                Console.Write(dynamicHandlers.GetHandlerCount('b'));
                return true;
            });

            //Start event loop
            StartEventLoop("continous dynamically multihandled", "Try 'b', 'a', 'r', exit the loop by pressing 'q' or 'x' -> .oOX", dynamic);
        }

        static void CreateGenericEventHandler()
        {
            //Create event handler collection
            GenericEventHandlers genericHandlers = new GenericEventHandlers();

            //Create event loop, inject handlers
            ContinousEventLoop generic = new ContinousEventLoop(genericHandlers);

            //Add event specific handlers for generic KeyPress event
            genericHandlers +=
                (pressed) =>
                {
                    Console.Write(char.ToUpper(pressed));
                    return true;
                };

            genericHandlers +=
                (pressed) =>
                {
                    return pressed != 'q';
                };

            //Start event loop
            StartEventLoop("generic", "Exit the loop by pressing 'q'", generic);
        }

        static void CreateMulticastEventHandler()
        {
            //Create event handler collection
            MulticastEventHandler multicastHandlers = new MulticastEventHandler();

            //Create event loop, inject handlers
            ContinousEventLoop multicast = new ContinousEventLoop(multicastHandlers);

            //Add event specific handlers for generic KeyPress event
            multicastHandlers.eventHandlers +=
                (pressed) =>
                {
                    Console.Write(char.ToUpper(pressed));
                    return false;
                };

            multicastHandlers.eventHandlers +=
                (pressed) =>
                {
                    return pressed != 'q';
                };

            //Start event loop
            StartEventLoop("multicast", "Exit the loop by pressing 'q'", multicast);
        }

        static void CreateRealEventHandler()
        {
            //Create event loop with real event
            RealEventLoop real = new RealEventLoop();

            //Add multiple handlers for the event
            //Handlers must conform signature described in method reference declaration
            //Multiple handlers can be added
            real.KeyEvent += (sender, eventArgs) => Console.Write(char.ToUpper(eventArgs.Char)); 
            real.KeyEvent += Real_Quit_KeyEventHandler;

            //Start event loop
            StartEventLoop("real", "Exit the loop by pressing 'q'", real);
        }

        private static void Real_Quit_KeyEventHandler(object sender, KeyEventArgs eventArgs)
        {
            if(eventArgs.Char == 'q')
            {
                ((RealEventLoop)sender).Terminate();
            }
        }

        static bool PrintOs()
        {
            Console.Write(".oOo.");
            return true;
        }

        static void StartEventLoop(string type, string message, IEventLoop eventLoop)
        {
            Console.WriteLine();
            Console.WriteLine("----------");
            Console.WriteLine("A(n) {0} event loop has been started to recognise key press events triggered by the user", type);

            Console.WriteLine(message);
            eventLoop.Run();
            Console.WriteLine();
            Console.WriteLine("Event loop has been terminated");
        }
    }
}
