﻿namespace Events
{
    interface IEventLoop
    {
        void Run();
    }
}
