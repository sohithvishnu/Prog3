﻿using System;

namespace Events
{
    class RealEventLoop : ContinousEventLoop
    {
        //Declare handler method reference type (describing handler signature)
        public delegate void KeyEventHandler(object sender, KeyEventArgs args);

        //Declare an event, which can be handled by handlers of specific method reference type
        public event KeyEventHandler KeyEvent;

        //Now we do not use aou custom event handler collections
        public RealEventLoop() : base(null)
        { }

        //A method to terminate the event loop from outside 
        //instead of having a control function inside
        public void Terminate()
        {
            nextIterationStep = false;
        }

        //Main event loop is overridden to eliminate call of control function returning bool 
        //The main content is the same, but instead of calling the control method, the event is fired
        public override void Run()
        {
            ResetMarker();

            nextIterationStep = true;

            while (nextIterationStep)
            {
                ShowLoopMarker();

                if (Console.KeyAvailable)
                {
                    ConsoleKeyInfo key = Console.ReadKey(false);

                    //If there are any handlers registered, fire the event (call handlers)
                    //Indeed, call all registered method references
                    //No need to handle individual calls, because event handlers have void return type
                    //therefore have no return value
                    KeyEvent?.Invoke(this, new KeyEventArgs(key.KeyChar));
                }
            }
        }
    }
}
