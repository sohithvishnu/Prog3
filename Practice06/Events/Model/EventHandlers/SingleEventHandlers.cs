﻿using System.Collections.Generic;

namespace Events
{
    class SingleEventHandlers : IKeyEventHandler
    {
        public delegate bool KeyPressHandler();

        private Dictionary<char, KeyPressHandler> eventHandlers = new Dictionary<char, KeyPressHandler>();

        public void SetEventHandler(char keyChar, KeyPressHandler handler)
        {
            eventHandlers[keyChar] = handler;
        }

        public bool HandleKeyEvent(char eventChar)
        {
            if (eventHandlers.ContainsKey(eventChar))
            {
                return eventHandlers[eventChar]();
            }

            return true;
        }
    }
}
