﻿using System.Collections.Generic;

namespace Events
{
    class GenericEventHandlers : IKeyEventHandler
    {
        public delegate bool KeyPressHandler(char pressed);

        private List<KeyPressHandler> keyPressEventHandlers = new List<KeyPressHandler>();

        #region Private methods are called from static (overloaded) operators

        private void AddAnyEventHandler(KeyPressHandler handler)
        {
            keyPressEventHandlers.Add(handler);
        }

        private void RemoveAnyEventHandler(KeyPressHandler handler)
        {
            keyPressEventHandlers.Remove(handler);
        }

        public bool HandleKeyEvent(char eventKey)
        {
            bool nextStep = true;

            foreach (KeyPressHandler handler in keyPressEventHandlers)
            {
                nextStep &= handler(eventKey);
            }

            return nextStep;
        }

        #endregion

        #region  Override + and - binary operators to let handlers added and removed easily

        //Note that :
        //  - first operand is a the handler collection to modify
        //  - second operand is the handler to add
        //  - return value is the modified handler collection - NO NEW instance has been created

        public static GenericEventHandlers operator +(GenericEventHandlers collection, KeyPressHandler newHandler)
        {
            collection.AddAnyEventHandler(newHandler);
            return collection;
        }

        public static GenericEventHandlers operator -(GenericEventHandlers collection, KeyPressHandler newHandler)
        {
            collection.RemoveAnyEventHandler(newHandler);
            return collection;
        }

        #endregion
    }
}
