﻿using System.Collections.Generic;

namespace Events
{
    class MultiEventHandlers : IKeyEventHandler
    {
        public delegate bool KeyPressHandler();

        private Dictionary<char, List<KeyPressHandler>> eventHandlerCollections = new Dictionary<char, List<KeyPressHandler>>();



        public void AddSpecificEventHandler(char keyChar, KeyPressHandler handler)
        {
            if (!eventHandlerCollections.ContainsKey(keyChar))
            {
                eventHandlerCollections[keyChar] = new List<KeyPressHandler>();
            }

            eventHandlerCollections[keyChar].Add(handler);
        }

        public void RemoveSpecificEventHandler(char keyChar, KeyPressHandler handler)
        {
            if (!eventHandlerCollections.ContainsKey(keyChar))
            {
                eventHandlerCollections[keyChar] = new List<KeyPressHandler>();
            }

            eventHandlerCollections[keyChar].Remove(handler);
        }

        public int GetHandlerCount(char keyChar)
        {
            if (eventHandlerCollections.ContainsKey(keyChar))
            {
                return eventHandlerCollections[keyChar].Count;
            }
            else
            {
                return 0;
            }
        }



        public bool HandleKeyEvent(char eventKey)
        {
            bool nextStep = true;
            if (eventHandlerCollections.ContainsKey(eventKey))
            {
                foreach (KeyPressHandler handler in eventHandlerCollections[eventKey])
                {
                    nextStep &= handler();
                }
            }
            return nextStep;
        }
    }
}
