﻿namespace Events
{
    class WiredEventHandler : IKeyEventHandler
    {
        public bool HandleKeyEvent(char eventKey)
        {
            return eventKey != 'q';
        }
    }
}
