﻿using System;
using System.Windows.Forms;

namespace ListBox
{
    public partial class mainForm : Form
    {
        public mainForm()
        {
            InitializeComponent();
        }

        private void split_Click(object sender, EventArgs e)
        {
            //Clear listbox items
            stringParts.Items.Clear();

            //Fill listbox items with parts of split by coma (,)
            string[] parts = fullString.Text.Split(',');
            foreach (var part in parts)
            {
                stringParts.Items.Add(part);
            }

            //Enable controls of next steps
            stringParts.Enabled = stringParts.Items.Count > 0;
            select.Enabled = stringParts.Enabled;
        }

        private void select_Click(object sender, EventArgs e)
        {
            //Check if there is an item selected
            if (stringParts.SelectedItem != null)
            {
                //If so, print a message
                selected.Text = (string)stringParts.SelectedItem;
                message.Text = string.Format("{0}. item has been selected", stringParts.SelectedIndex + 1);
            }
            else
            {
                selected.Text = "";
                message.Text = "Please select an item";

                //Otherwise show a MessageBox
                MessageBox.Show(
                    "There is no selected item",
                    "Info",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information
                );
            }
        }
    }
}
