﻿namespace ListBox
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.fullString = new System.Windows.Forms.TextBox();
            this.stringParts = new System.Windows.Forms.ListBox();
            this.select = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.selected = new System.Windows.Forms.TextBox();
            this.split = new System.Windows.Forms.Button();
            this.message = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Entered text:";
            // 
            // fullString
            // 
            this.fullString.Location = new System.Drawing.Point(120, 12);
            this.fullString.Name = "fullString";
            this.fullString.Size = new System.Drawing.Size(649, 22);
            this.fullString.TabIndex = 1;
            // 
            // stringParts
            // 
            this.stringParts.Enabled = false;
            this.stringParts.FormattingEnabled = true;
            this.stringParts.ItemHeight = 16;
            this.stringParts.Location = new System.Drawing.Point(120, 40);
            this.stringParts.Name = "stringParts";
            this.stringParts.Size = new System.Drawing.Size(163, 196);
            this.stringParts.TabIndex = 2;
            // 
            // select
            // 
            this.select.Enabled = false;
            this.select.Location = new System.Drawing.Point(362, 91);
            this.select.Name = "select";
            this.select.Size = new System.Drawing.Size(93, 30);
            this.select.TabIndex = 3;
            this.select.Text = "Select";
            this.select.UseVisualStyleBackColor = true;
            this.select.Click += new System.EventHandler(this.select_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(289, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 17);
            this.label2.TabIndex = 4;
            this.label2.Text = "Selected:";
            // 
            // selected
            // 
            this.selected.Enabled = false;
            this.selected.Location = new System.Drawing.Point(362, 127);
            this.selected.Name = "selected";
            this.selected.ReadOnly = true;
            this.selected.Size = new System.Drawing.Size(406, 22);
            this.selected.TabIndex = 5;
            // 
            // split
            // 
            this.split.Location = new System.Drawing.Point(676, 40);
            this.split.Name = "split";
            this.split.Size = new System.Drawing.Size(93, 30);
            this.split.TabIndex = 6;
            this.split.Text = "Split";
            this.split.UseVisualStyleBackColor = true;
            this.split.Click += new System.EventHandler(this.split_Click);
            // 
            // message
            // 
            this.message.AutoSize = true;
            this.message.Location = new System.Drawing.Point(365, 152);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(87, 17);
            this.message.TabIndex = 7;
            this.message.Text = "No message";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(499, 47);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "use coma (,) as separator";
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 250);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.message);
            this.Controls.Add(this.split);
            this.Controls.Add(this.selected);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.select);
            this.Controls.Add(this.stringParts);
            this.Controls.Add(this.fullString);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form with ListBox";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox fullString;
        private System.Windows.Forms.ListBox stringParts;
        private System.Windows.Forms.Button select;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox selected;
        private System.Windows.Forms.Button split;
        private System.Windows.Forms.Label message;
        private System.Windows.Forms.Label label3;
    }
}

