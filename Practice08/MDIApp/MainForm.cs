﻿using System;
using System.Windows.Forms;

namespace MDIApp
{
    public partial class MainForm : Form
    {
        private int docCount = 0;

        public MainForm()
        {
            InitializeComponent();
        }

        private void addDocumentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            docCount++;

            DocumentForm newDocument = new DocumentForm(
                String.Format("A(z) {0}. dokumentum tartalma", docCount)
                );

            if (this.IsMdiContainer)
            {
                newDocument.MdiParent = this;
            }

            newDocument.Text = String.Format("{0}. dokumentum ablak", docCount);
            newDocument.Show();
        }

        private void mosaicToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.Cascade);
        }

        private void horizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void verticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.ActiveMdiChild != null)
            {
                this.ActiveMdiChild.Close();
            }
            else
            {
                Close();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = MessageBox.Show("Really?", "Quit", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes;
        }
    }
}
