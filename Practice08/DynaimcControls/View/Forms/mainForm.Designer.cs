﻿namespace DynaimcControls
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labeledText3 = new DynaimcControls.LabeledText();
            this.labeledText1 = new DynaimcControls.LabeledText();
            this.labeledText2 = new DynaimcControls.LabeledText();
            this.SuspendLayout();
            // 
            // labeledText3
            // 
            this.labeledText3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labeledText3.EnteredText = "MyText 1";
            this.labeledText3.Label = "Text 1";
            this.labeledText3.Location = new System.Drawing.Point(12, 12);
            this.labeledText3.Name = "labeledText3";
            this.labeledText3.Size = new System.Drawing.Size(225, 56);
            this.labeledText3.TabIndex = 2;
            // 
            // labeledText1
            // 
            this.labeledText1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labeledText1.EnteredText = "MyText 2";
            this.labeledText1.Label = "Text 2";
            this.labeledText1.Location = new System.Drawing.Point(243, 12);
            this.labeledText1.Name = "labeledText1";
            this.labeledText1.Size = new System.Drawing.Size(225, 56);
            this.labeledText1.TabIndex = 3;
            // 
            // labeledText2
            // 
            this.labeledText2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.labeledText2.EnteredText = "MyText 3";
            this.labeledText2.Label = "Text 3";
            this.labeledText2.Location = new System.Drawing.Point(474, 12);
            this.labeledText2.Name = "labeledText2";
            this.labeledText2.Size = new System.Drawing.Size(225, 56);
            this.labeledText2.TabIndex = 4;
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 509);
            this.Controls.Add(this.labeledText2);
            this.Controls.Add(this.labeledText1);
            this.Controls.Add(this.labeledText3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main form";
            this.ResumeLayout(false);

        }

        #endregion
        private LabeledText labeledText3;
        private LabeledText labeledText1;
        private LabeledText labeledText2;
    }
}

