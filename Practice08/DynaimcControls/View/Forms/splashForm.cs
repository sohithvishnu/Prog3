﻿using System;
using System.Windows.Forms;

namespace DynaimcControls
{
    public partial class splashForm : Form
    {
        public splashForm()
        {
            InitializeComponent();

            //Set size of window to a specific value
            Size = new System.Drawing.Size(600, 600);
        }

        private void closeTimer_Tick(object sender, EventArgs e)
        {
            //Close window on elapsed timer interval
            Close();
        }
    }
}
