﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MeetingScheduler
{
    public partial class MainForm : Form
    {
        Timer ClockUpdater = new Timer();

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ValidateSchedule();

            UpdateClock();

            ClockUpdater.Interval = 1000;
            ClockUpdater.Tick += ClockUpdater_Tick;
            ClockUpdater.Enabled = true;

            Message.Text = "";
        }

        private void ClockUpdater_Tick(object sender, EventArgs e)
        {
            UpdateClock();
            UpdateMessage();
        }

        private void UpdateClock()
        {
            Current.Text = DateTime.Now.ToString("F");
        }

        private void Date_ValueChanged(object sender, EventArgs e)
        {
            ValidateSchedule();
            UpdateMessage();
        }

        private void ValidateSchedule()
        {
            DateTime selected = GetSelectedDateTime();

            Selected.Text = selected.ToString("F");
        }

        private void UpdateMessage()
        {
            DateTime selected = GetSelectedDateTime();

            if(selected.CompareTo(DateTime.Now)< 0)
            {
                Message.Text = "Ezt lekésted!";
            }
            else
            {
                TimeSpan diff = selected.Subtract(DateTime.Now);
                Message.Text = String.Format(
                    "Még {0} nap {1} óra {2} perc",
                    diff.Days, diff.Hours, diff.Minutes
                );
            }
        }

        private DateTime GetSelectedDateTime()
        {
            DateTime selected = Date.Value.Date;
            selected = selected.AddHours(Time.Value.Hour);
            selected = selected.AddMinutes(Time.Value.Minute);
            selected = selected.AddSeconds(Time.Value.Second);
            return selected;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult response = MessageBox.Show(
                "Biztosan ki akar lépni?", "Figyelem!", MessageBoxButtons.YesNo
            );

            e.Cancel = response != DialogResult.Yes;
        }
    }
}
