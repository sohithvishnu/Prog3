﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaOrder
{
    class PizzaStorage
    {
        private List<Pizza> pizzas;

        #region Element access

        public int Count
        {
            get { return pizzas.Count; }
        }

        public Pizza this[int index]
        {
            get { return pizzas[index]; }
        }

        #endregion

        public PizzaStorage()
        {
            pizzas = new List<Pizza>();
        }

        #region Add elements

        public void Add(Pizza newPizza)
        {
            pizzas.Add(newPizza);
        }

        public void LoadFromFile(string fileName, IPizzaParser parser)
        {
            pizzas.Clear();
            string multilineTextInput = "";
            using (StreamReader sr = new StreamReader(fileName))
            {
                multilineTextInput = sr.ReadToEnd();
            }
            pizzas.AddRange(parser.Parse(multilineTextInput));
        }

        #endregion
    }
}
