﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace PizzaOrder
{
    public partial class mainForm : Form
    {
        private int meretKicsi = 32, meretNagy = 45;

        private const int yDiff = 41;

        private PizzaStorage pizzaStorage = new PizzaStorage();
        private List<PizzaControl> pizzaControls = new List<PizzaControl>();

        public mainForm()
        {
            InitializeComponent();
            this.CenterToScreen();

            SetControlsVisibility(false);
        }

        private void Torol_Click(object sender, EventArgs e)
        {
            ResetUI();
        }

        private void ResetUI()
        {
            foreach (PizzaControl ctrl in pizzaControls)
                ctrl.Clear();

            fizetendo.Clear();
        }

        private void Szamol_Click(object sender, EventArgs e)
        {
            try
            {
                int osszeg = 0;

                foreach (PizzaControl ctrl in pizzaControls)
                {
                    osszeg += ctrl.GetTotalPrice();
                }

                fizetendo.Text = osszeg.ToString();
            }
            catch (FormatException)
            {
                MessageBox.Show("Hibás mennyiség formátuma!", "Figyelem!");
            }
            catch (ArgumentOutOfRangeException)
            {
                MessageBox.Show("A mennyiség legyen pozitív!", "Figyelem!");
            }
        }

        private void AdatBevitel_Click(object sender, EventArgs e)
        {
            if(LoadFile())
            {
                InitUI();
                ResetUI();
                SetControlsVisibility(true);
            }
        }

        private bool LoadFile()
        {
            try
            {
                DialogResult selected = openFileDialog.ShowDialog();
                if (selected == DialogResult.OK)
                {
                    pizzaStorage.LoadFromFile(
                        openFileDialog.FileName,
                        new PizzaListParser()
                    );

                    return true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Fájl olvasási hiba!", "Figyelem!");
            }

            return false;
        }

        private void InitUI()
        {
            lblKicsi.Text = meretKicsi.ToString() + " cm";
            lblNagy.Text = meretNagy.ToString() + " cm";

            int rowCount = 0;
            dynamicContentContainer.Controls.Clear();
            for (int i = 0; i < pizzaStorage.Count; i++)
            {
                pizzaControls.Add(
                    new PizzaControl(
                        dynamicContentContainer,
                        rowCount * yDiff,
                        pizzaStorage[i]
                    )
                );

                rowCount++;
            }
        }

        private void mainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Biztossan ki szeretne lépni?", "Figyelem!", MessageBoxButtons.YesNo) != DialogResult.Yes)
            {
                e.Cancel = true;
            }
        }

        private void CloseWindow_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void SetControlsVisibility(bool newVisibility)
        {
            BackImage.Visible = !newVisibility;
            lblKicsi.Visible = newVisibility;
            lblNagy.Visible = newVisibility;
            dynamicContentContainer.Visible = newVisibility;
            szamol.Visible = newVisibility;
            torol.Visible = newVisibility;
            fizetendoLabel.Visible = newVisibility;
            fizetendo.Visible = newVisibility;
            adatBevitel.Visible = !newVisibility;
        }
    }
}
