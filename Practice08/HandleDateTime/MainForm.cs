﻿using System;
using System.Globalization;

using System.Windows.Forms;

namespace HandleDateTime
{
    public partial class MainForm : Form
    {
        Timer dateRefreshTimer;

        public MainForm()
        {
            InitializeComponent();

            dateRefreshTimer = new Timer();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            //System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("hu-HU");

            CurrentDateTime.Text = DateTime.Now.ToString("F");
            Message.Text = "";
            DayOfYear.Text = "";
            EndDate.Text = "";

            dateRefreshTimer.Interval = 1000;
            dateRefreshTimer.Tick += DateRefreshTimer_Tick;
            dateRefreshTimer.Enabled = true;
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult response = MessageBox.Show(
                "Biztosan ki akar lépni?", 
                "Figyelem!", 
                MessageBoxButtons.YesNo
            );

            e.Cancel = response != DialogResult.Yes;
        }

        private void DateRefreshTimer_Tick(object sender, EventArgs e)
        {
            CurrentDateTime.Text = DateTime.Now.ToString("F");
        }

        private void BirthDate_Leave(object sender, EventArgs e)
        {
            CheckAge();
        }

        private void BirthDate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CheckAge();
            }
        }

        private void customDate_ValueChanged(object sender, EventArgs e)
        {
            CalcEndDate();
        }

        private void Interval_TextChanged(object sender, EventArgs e)
        {
            CalcEndDate();
        }

        private void CalcEndDate()
        {
            try
            {
                int days = int.Parse(Interval.Text);
                DayOfYear.Text = customDate.Value.DayOfYear.ToString();
                EndDate.Text = customDate.Value.AddDays(days).ToString("D");
            }
            catch (FormatException)
            {
                EndDate.Text = "";
            }
        }

        private void CheckAge()
        {
            DateTime date;
            try
            {
                date = DateTime.Parse(BirthDate.Text);
                Age.Text = (DateTime.Now.Year - date.Year).ToString();
                BirthDayOfWeek.Text = CultureInfo.CurrentCulture.DateTimeFormat.GetDayName(date.DayOfWeek);

                if (DateTime.Now.Month == date.Month && DateTime.Now.Day == date.Day)
                {
                    Message.Text = "Boldog szülinapot!";
                }
                else
                {
                    Message.Text = "Önnek ma nincs születésnapja!";
                }
            }
            catch (FormatException)
            {
                MessageBox.Show("Hibás dátum");
                BirthDate.Focus();
            }
        }
    }
}
