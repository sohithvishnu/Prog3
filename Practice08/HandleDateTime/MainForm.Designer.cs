﻿namespace HandleDateTime
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.CurrentDateTime = new System.Windows.Forms.Label();
            this.BirthDate = new System.Windows.Forms.MaskedTextBox();
            this.Age = new System.Windows.Forms.TextBox();
            this.BirthDayOfWeek = new System.Windows.Forms.TextBox();
            this.customDate = new System.Windows.Forms.DateTimePicker();
            this.DayOfYear = new System.Windows.Forms.Label();
            this.Interval = new System.Windows.Forms.TextBox();
            this.EndDate = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Message = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Születési dátum:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(141, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Pontos dátum és idő:";
            // 
            // CurrentDateTime
            // 
            this.CurrentDateTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CurrentDateTime.ForeColor = System.Drawing.Color.Red;
            this.CurrentDateTime.Location = new System.Drawing.Point(166, 9);
            this.CurrentDateTime.Name = "CurrentDateTime";
            this.CurrentDateTime.Size = new System.Drawing.Size(483, 27);
            this.CurrentDateTime.TabIndex = 3;
            this.CurrentDateTime.Text = "Current date";
            // 
            // BirthDate
            // 
            this.BirthDate.Location = new System.Drawing.Point(137, 74);
            this.BirthDate.Mask = "0000-00-00";
            this.BirthDate.Name = "BirthDate";
            this.BirthDate.Size = new System.Drawing.Size(88, 22);
            this.BirthDate.TabIndex = 4;
            this.BirthDate.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BirthDate_KeyDown);
            this.BirthDate.Leave += new System.EventHandler(this.BirthDate_Leave);
            // 
            // Age
            // 
            this.Age.Location = new System.Drawing.Point(260, 74);
            this.Age.Name = "Age";
            this.Age.ReadOnly = true;
            this.Age.Size = new System.Drawing.Size(100, 22);
            this.Age.TabIndex = 5;
            this.Age.TabStop = false;
            // 
            // BirthDayOfWeek
            // 
            this.BirthDayOfWeek.Location = new System.Drawing.Point(386, 74);
            this.BirthDayOfWeek.Name = "BirthDayOfWeek";
            this.BirthDayOfWeek.ReadOnly = true;
            this.BirthDayOfWeek.Size = new System.Drawing.Size(149, 22);
            this.BirthDayOfWeek.TabIndex = 6;
            this.BirthDayOfWeek.TabStop = false;
            // 
            // customDate
            // 
            this.customDate.Location = new System.Drawing.Point(22, 174);
            this.customDate.Name = "customDate";
            this.customDate.Size = new System.Drawing.Size(230, 22);
            this.customDate.TabIndex = 9;
            this.customDate.ValueChanged += new System.EventHandler(this.customDate_ValueChanged);
            // 
            // DayOfYear
            // 
            this.DayOfYear.AutoSize = true;
            this.DayOfYear.Location = new System.Drawing.Point(111, 150);
            this.DayOfYear.Name = "DayOfYear";
            this.DayOfYear.Size = new System.Drawing.Size(46, 17);
            this.DayOfYear.TabIndex = 10;
            this.DayOfYear.Text = "label3";
            // 
            // Interval
            // 
            this.Interval.Location = new System.Drawing.Point(258, 174);
            this.Interval.Name = "Interval";
            this.Interval.Size = new System.Drawing.Size(100, 22);
            this.Interval.TabIndex = 11;
            this.Interval.Text = "0";
            this.Interval.TextChanged += new System.EventHandler(this.Interval_TextChanged);
            // 
            // EndDate
            // 
            this.EndDate.AutoSize = true;
            this.EndDate.Location = new System.Drawing.Point(380, 177);
            this.EndDate.Name = "EndDate";
            this.EndDate.Size = new System.Drawing.Size(46, 17);
            this.EndDate.TabIndex = 12;
            this.EndDate.Text = "label3";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(257, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 17);
            this.label3.TabIndex = 13;
            this.label3.Text = "Kor";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(383, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(153, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "Ezen a napon született";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(379, 150);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 17);
            this.label5.TabIndex = 15;
            this.label5.Text = "Új dátum";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(255, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 17);
            this.label6.TabIndex = 16;
            this.label6.Text = "Intervallum [nap]";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(19, 150);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(86, 17);
            this.label7.TabIndex = 17;
            this.label7.Text = "Az év napja:";
            // 
            // Message
            // 
            this.Message.AutoSize = true;
            this.Message.Location = new System.Drawing.Point(134, 113);
            this.Message.Name = "Message";
            this.Message.Size = new System.Drawing.Size(121, 17);
            this.Message.TabIndex = 8;
            this.Message.Text = "Birthday message";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(661, 218);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.EndDate);
            this.Controls.Add(this.Interval);
            this.Controls.Add(this.DayOfYear);
            this.Controls.Add(this.customDate);
            this.Controls.Add(this.Message);
            this.Controls.Add(this.BirthDayOfWeek);
            this.Controls.Add(this.Age);
            this.Controls.Add(this.BirthDate);
            this.Controls.Add(this.CurrentDateTime);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dátum és idő kezelése";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label CurrentDateTime;
        private System.Windows.Forms.MaskedTextBox BirthDate;
        private System.Windows.Forms.TextBox Age;
        private System.Windows.Forms.TextBox BirthDayOfWeek;
        private System.Windows.Forms.DateTimePicker customDate;
        private System.Windows.Forms.Label DayOfYear;
        private System.Windows.Forms.TextBox Interval;
        private System.Windows.Forms.Label EndDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label Message;
        private System.Windows.Forms.Timer timer1;
    }
}

