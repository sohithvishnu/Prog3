﻿using System;

namespace _00_OOP_tools
{
    class Program
    {
        static void Main(string[] args)
        {
            //Can not create instance of an abstract class
            //Machine m = new Machine(10, 20, 30);
            //Cutter c = new Cutter(10, 20, 30);
            //Printer p = new Printer(10, 20, 30);

            LaserCutter lc = new LaserCutter(10, 20, 30);
            CNCMilling cncm = new CNCMilling(10, 20, 30);

            PlasticPrinter pp = new PlasticPrinter(10, 20, 30);
            MetalPrinter mp = new MetalPrinter(10, 20, 30);

            lc.CreateWorkpiece();
            cncm.CreateWorkpiece();
            pp.CreateWorkpiece();
            mp.CreateWorkpiece();

            Console.ReadKey();

            Console.WriteLine("----====    CreateWorkPiece after Cast    ====----");

            //Cast all instances to Machine
            //use Machine interface to access all instances

            //Machine class name is pale blue, becase it does not change accessed method
            //Behaviour is inherited and specializer by 'override'
            ((Machine)lc).CreateWorkpiece();
            ((Machine)cncm).CreateWorkpiece();

            //Machine class name is blue, becase it DOES change accessed method
            //Behaviour SIGNATURE inherited but the behaviour itself is changed by 'new'
            //behaviour of Machine is not guaranteed
            ((Machine)pp).CreateWorkpiece();
            ((Machine)mp).CreateWorkpiece();

            Console.WriteLine("----====    Reset after Cast    ====----");
            //New Reset method
            pp.Reset();
            ((Machine)pp).Reset();
        }
    }
}
