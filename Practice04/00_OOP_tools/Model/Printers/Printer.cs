﻿using System;

namespace _00_OOP_tools
{
    //Class is still abstract, because printers of specific make and model exist
    public abstract class Printer : Machine
    {
        //Constructor calls parent (base) constructor
        public Printer(int height, int width, int length) : base(height, width, length)
        {

        }

        //Creation method is more specific
        public override void CreateWorkpiece()
        {
            Console.WriteLine("Build workpiece from source material");
        }
    }
}
