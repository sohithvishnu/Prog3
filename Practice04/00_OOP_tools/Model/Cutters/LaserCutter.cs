﻿using System;

namespace _00_OOP_tools
{
    public class LaserCutter : Cutter
    {
        //Constructor calls parent (base) constructor
        public LaserCutter(int height, int width, int length) : base(height, width, length)
        {

        }

        //Creation method is even more specific
        //Inherited behaviour is overridden
        public override void CreateWorkpiece()
        {
            Console.WriteLine("Cut workpiece with LASER from base material");
        }
    }
}
