﻿using System;

namespace Shells.Model
{
    class Rod : Cylinder
    {
        public int Density { get; set; }

        virtual public double Mass
        {
            get
            {
                if (Density > 0)
                {
                    return Volume * Density;
                }
                else return 0;
            }
        }

        public Rod(int r, int h, int d) : base(r, h)
        {
            this.Density = d;
        }

        public override string ToString()
        {
            return String.Format("Rod radius : {0}, height: {1}, volume {2:0.00}, weight: {3:0.00}", 
                Radius, Height, Volume, Mass
            );
        }
    }
    
}
