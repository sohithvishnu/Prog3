﻿using System;

namespace Shells.Model
{
    abstract class Cylinder
    {
        public int Radius { get; set; }
        public int Height { get; set; }

        virtual public double Volume
        {
            get
            {
                return Math.Pow(Radius, 2) * Math.PI * Height;
            }
        }

        public Cylinder(int r, int h)
        {
            this.Radius = r;
            this.Height = h;

        }

        public override string ToString()
        {
            return String.Format("Cylinder radius: {0}, height: {1}, volume: {2:0.00}", 
                Radius, Height, Volume
            ) ;
        }
    }
}
