﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CatDogContest.Model
{
    class FileParser3Line : FileParser
    {
        public override List<Animal> ParseFile(string fileName)
        {
            List<Animal> competitors = new List<Animal>();
            StreamReader reader = new StreamReader(fileName);

            string race;

            while (!reader.EndOfStream)
            {
                race = reader.ReadLine();

                switch (race)
                {
                    case "kutya":
                        competitors.Add(
                            new Dog(
                                competitors.Count + 1,
                                reader.ReadLine(),
                                Convert.ToInt32(reader.ReadLine()),
                                Convert.ToInt32(reader.ReadLine())
                            )
                        );
                        break;
                    case "macska":
                        competitors.Add(
                            new Cat(
                                competitors.Count + 1,
                                reader.ReadLine(),
                                Convert.ToInt32(reader.ReadLine()),
                                reader.ReadLine() == "true"
                            )
                        );
                        break;
                }
            }

            reader.Close();

            return competitors;
        }
    }
}
