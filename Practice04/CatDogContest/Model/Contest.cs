﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CatDogContest.Model
{
    class Contest
    {
        //Value of maxage is checked in the setter, therefore explicit getter and setter were created.
        //Important: the value can not be set from outside the object -> setter is private
        private int maxAge;
        public int MaxAge {
            get { return maxAge; }
            private set {
                if (value < 1) throw new Exception("Maximum age is too small");
                maxAge = value;
            }
        }

        //Value of CurrentYear also has to be checked, but...
        //  the data member (which stores the data) is accessible diretly in the class, it can be written not using the setter
        //  therefore usage of auto property with private setter and value check on initialization (in Constructor)
        //  has similar result, but much less code
        public int CurrentYear { get; private set; }

        //Points have to be set in pair, so use SetPointLimits setter to check their relations
        public int PointsMin { get; private set; }
        public int PointsMax { get; private set; }

        private List<Animal> competitors;
        private Random random;

        private void SetPointLimits(int pointsMin, int pointsMax)
        {
            if (pointsMin < 1) throw new Exception("Minimum point is too small");
            if (pointsMax < pointsMin) throw new Exception("Maximum point is too small");
            PointsMin = pointsMin;
            PointsMax = pointsMax;
        }

        public Contest(int pointsMin, int pointsMax, int maxAge, int currentYear)
        {
            SetPointLimits(pointsMin, pointsMax);

            //Check value on initialization
            if (currentYear < 2020) throw new Exception("Current year is too small");
            if (currentYear > 2030) throw new Exception("Current year is too big");

            MaxAge = maxAge;
            CurrentYear = currentYear;

            competitors = new List<Animal>();
            random = new Random();
    }

        public void RegistrationFromFile(FileParser parser, string fileName)
        {
            competitors.AddRange(parser.ParseFile(fileName));
        }

        public void EvaluateCompetitors()
        {
            int beautyPoints, behaviourPoints;
            foreach (Animal competitor in competitors)
            {
                beautyPoints = random.Next(PointsMin, PointsMax+1);
                behaviourPoints = random.Next(PointsMin, PointsMax+1);
                Console.WriteLine(
                        "A #{0} rajtszámú versenyző értékelése: Szépség={1} Viselekdés={2}",
                        competitor.Id, beautyPoints, behaviourPoints
                    );
                competitor.Evaluation(beautyPoints, behaviourPoints);
            }
        }

        #region List filters

        public Animal GetOneWinner()
        {
            Animal winner = competitors[0];
            foreach(Animal competitor in competitors)
            {
                if(winner.TotalPoints < competitor.TotalPoints)
                {
                    winner = competitor;
                }
            }

            return winner;
        }

        //Get ALL competitors who are tied for the lead
        //Read the list only 1 time
        //The list is not sorted (have to read the whole list)
        public List<Animal> GetAllWinners()
        {
            List<Animal> winners = new List<Animal>();
            foreach (Animal competitor in competitors)
            {
                //If no winner stored or a better item found, clear the list
                if (winners.Count > 0 && winners[0].TotalPoints < competitor.TotalPoints)
                {
                    winners.Clear();
                }

                //If no winner stored or tied with first winner, store the new winner
                if (winners.Count == 0 || winners[0].TotalPoints == competitor.TotalPoints)
                {
                    winners.Add(competitor);
                }
            }

            return winners;
        }

        public Animal Find(int id)
        {
            return competitors.Find(x => x.Id == id);
        }

        #endregion

        #region List competitors

        public void ListCompetitors()
        {
            ListCompetitors(competitors);
        }

        private void ListCompetitors(List<Animal> competitorsToList)
        {
            foreach (Animal competitor in competitorsToList)
            {
                Console.WriteLine(competitor);
            }
        }

        private List<Animal> FindAllByPoints(int points)
        {
            List<Animal> competitorsFound = new List<Animal>();
            foreach (Animal competitor in competitors)
            {
                if (points == competitor.TotalPoints)
                {
                    competitorsFound.Add(competitor);
                }
            }

            return competitorsFound;
        }

        public void ListAllWinners1()
        {
            ListCompetitors(
                GetAllWinners()
            );
        }

        public void ListAllWinners2()
        {
            ListCompetitors(
                FindAllByPoints(
                    GetOneWinner().TotalPoints
                )
            );
        }

        #endregion

        #region Sort

        public void SortByPoints()
        {
            competitors.Sort((x, y) => -x.TotalPoints.CompareTo(y.TotalPoints));
        }

        public void SortById()
        {
            competitors.Sort((x, y) => x.Id.CompareTo(y.Id));
        }

        #endregion

        
    }
}
