﻿using System;

namespace CatDogContest.Model
{
    class Cat : Animal
    {
        public bool HasCarrier { get; set; }

        public Cat(int id, string name, int birthYear, bool hasCarrier) : base(id, name, birthYear)
        {
            HasCarrier = hasCarrier;
        }

        override public int TotalPoints
        {
            get
            {
                return HasCarrier ? base.TotalPoints : 0;
            }
        }

        public override string ToString()
        {
            if (!isEvaluated)
            {
                return String.Format("A {0} nevű, #{1} rajtszámú macska még nincs pontozva", Name, Id);
            }
            else
            {
                return String.Format("A {0} nevű, #{1} rajtszámú macska pontszáma: {2}", Name, Id, TotalPoints);
            }
        }
    }
}
