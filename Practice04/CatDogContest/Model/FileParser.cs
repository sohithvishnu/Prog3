﻿using System.Collections.Generic;

namespace CatDogContest.Model
{
    abstract class FileParser
    {
        public abstract List<Animal> ParseFile(string fileName);
    }
}
