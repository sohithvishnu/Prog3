﻿using System;

namespace CatDogContest.Model
{
    class Animal
    {
        static bool isInitialized = false;
        static int maxAge;
        static int currentYear;

        static public void Init(int maxAge, int currentYear)
        {
            Animal.maxAge = maxAge;
            Animal.currentYear = currentYear;
            Animal.isInitialized = true;
        }

        public int Id { get; private set; }
        public string Name { get; private set; }
        public int BirthYear { get; private set; }
        public int BeautyPoints { get; private set; }
        public int BehaviourPoints { get; private set; }
        public bool isEvaluated { get; private set; }

        public int Age
        {
            get
            {
                return currentYear - BirthYear;
            }
        }

        virtual public int TotalPoints {
            get
            {
                int points;
                if (Age > maxAge)
                {
                    points = 0;
                }
                else
                {
                    points = (maxAge - Age) * BeautyPoints
                        + Age * BehaviourPoints;
                }

                return points;
            }
        }

        public Animal(int id, string name, int birthYear)
        {
            if (!isInitialized)
                throw new Exception("Please Init class first!");

            Id = id;
            Name = name;
            BirthYear = birthYear;
            isEvaluated = false;
        }

        public void Evaluation(int beautyPoint, int behaviourPoint)
        {
            BeautyPoints = beautyPoint;
            BehaviourPoints = behaviourPoint;
            isEvaluated = true;
        }

        public override string ToString()
        {
            string serializedOutput;
            if (!isEvaluated)
            {
                serializedOutput = String.Format("{0}(#{1}) még nincs pontozva", Name, Id);
            }
            else
            {
                serializedOutput = String.Format("{0}(#{1}) állat pontszáma: {2}",
                    Name, Id, TotalPoints
                    );
            }

            return serializedOutput;
        }
    }
}
