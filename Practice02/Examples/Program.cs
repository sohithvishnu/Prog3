﻿using System;

namespace Examples
{
    class Program
    {
        static void Main(string[] args)
        {
            //Calling static method with parameters
            //(When calling method of the SAME class, class name specification is not required)
            int c = Program.Calc(2, 3);

            //When setting class (static) property of another class, the class name IS required
            Person.MaxHeight = 250;

            //Create new instance of Person class
            Person player = new Person("Gustav", 182);

            //Setting instance property
            player.JumpHeight = 85;

            //Print string representation of the object
            //  This is returned by ToString instance method
            //  ToString is called automatically by the .Net Virtual Machine
            Console.WriteLine(player);

            //Calling instance method
            player.TeachTactics("Winner tactics");

            //Check state changes
            Console.WriteLine(player);

            //Using named parameters in method call (called method is the constructor)
            //    order of parameters can be custom, they are identified by name
            //Instance creation and property setting are merged into one statement
            //    by adding a block right after the constructor parameters
            Person anotherPlayer = new Person(height: 190, name: "William")
            {
                JumpHeight = 98
            };

            //Check state of the object
            Console.WriteLine(anotherPlayer);
        }

        //Very simple static method just to present static calls
        static int Calc(int a, int b)
        {
            return a + b;
        }
    }
}
