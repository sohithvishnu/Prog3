﻿namespace _00_TypeParameter
{
    class LengthChecker
    {
        //Creating and overloading a method to get an object with longer string represenataion

        //for integers
        public static int LongerString(int a, int b)
        {
            return a.ToString().Length > a.ToString().Length ? a : b;
        }

        //for doubles
        public static double LongerString(double a, double b)
        {
            return a.ToString().Length > a.ToString().Length ? a : b;
        }

        //Becase all object can be converted to string, 
        //a generic method can be declared to perform the task
        public static T LongerString<T>(T a, T b)
        {
            return a.ToString().Length > a.ToString().Length ? a : b;
        }
    }
}
