﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _00_TypeParameter
{
    class Program
    {
        static void Main(string[] args)
        {
            CheckLengths();

            UseStorages();
        }

        private static void CheckLengths()
        {
            int longerInt = LengthChecker.LongerString(5, 45);
            double longerDouble = LengthChecker.LongerString(4.0, 2.45);

            int longerInt2 = LengthChecker.LongerString(5, 45);
            double longerDouble2 = LengthChecker.LongerString(4.0, 2.45);

            //Usage of generic method, type parameter is specified
            //type parameter is pale, because can be inferred from actual parameters (input and return type)
            //type parameter can be ommitted
            float longerFloat = LengthChecker.LongerString<float>(6.4f, 422.9f);
            bool longerBool = LengthChecker.LongerString<bool>(true, false);

            ///Usage of generic method, type parameter is inferred from actual parameters
            string longerString = LengthChecker.LongerString("apple", "raspberry");
        }

        private static void UseStorages()
        {
            //Create an instance of generic class with type parameter
            MultiItemStorage<int> intStorage = new MultiItemStorage<int>(5);
            intStorage.AddItem(2);
            intStorage.AddItem(6);
            intStorage.AddItem(4);
            intStorage.AddItem(1);
            intStorage.Print("Int list");
            Console.WriteLine("Int list size: {0}", intStorage.GetData().Length);

            //Create an instance of generic class with type parameter
            MultiItemStorage<double> doubleStorage = new MultiItemStorage<double>(10);
            doubleStorage.AddItem(3.2);
            doubleStorage.AddItem(6.8);
            doubleStorage.AddItem(4.4);
            doubleStorage.AddItem(1.1);
            doubleStorage.Print("Double list");
            doubleStorage.PrintAll("Full double list");
            Console.WriteLine("Double list size: {0}", doubleStorage.GetData().Length);
        }
    }
}
