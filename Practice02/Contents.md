# Contents of Practice 02
In this lecture you see how to:
- declare class with data members and behaviours
- declare properties (default, computed and automatic)
- instantiate a class to have an object
- declare method parameters
- overload methods considering method signatures
  
## 01 - Examples  
Examples of:  
- static and instance method calls
- class declaration
- property declaration
- automatic properties
- instantiation
- instance method calls
- instance state management
  
## 02 - Animal contest  
A console application helps to organize an animal contest.  
Animals under a certain age compete with their behaviour and beauty.

Behaviour points and beauty points are given by a jury.

If the animal is under a certain age (maxAge), then  
`TotalPoints: (maxAge - Age) * BeautyPoints + Age * BehaviourPoints`  
if the animal is older, then can participate but can not compete, has 0 points.  
  
The application does not store all contestants, but reads the properties and evaluation of the current competitor and stores the winner.  
The first with the most points is the winner.  
The application also calculates the number of competitors participated and their average score.  
To calculate this, total points also have to be summarized.
  
The project contains:
- Input.cs - class encapsulating console input functions. These functions are stateless, therefore the methods implementig them are static.
- Contest.cs - class encapsulating data and operations related to the contest
- Animal.cs - class encapsulating data and operations of a participant