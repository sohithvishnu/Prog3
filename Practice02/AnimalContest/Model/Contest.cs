﻿using System;

namespace AnimalContest.Model
{
    class Contest
    {
        //Generic, input related (not a contest parameter)
        const int BIRTH_MIN = 1900;
        const int BIRTH_MAX = 2030;

        //Create a pseudo-random sequence seeded by the current system clock
        static Random random = new Random();

        //Private data members
        private bool randomEvaluation;
        private int pointMin;
        private int pointMax;
        private int currentId;

        public Contest(int currYear, int maxAge, bool randomEvaluation, int pointMin, int pointMax)
        {
            //Store parameters in Animal class
            Animal.Init(maxAge, currYear);

            this.randomEvaluation = randomEvaluation;
            this.pointMin = pointMin;
            this.pointMax = pointMax;

            currentId = 0;
        }

        public void Start()
        {
            Animal animal;
            Animal winner = null;
            bool hasAnotherAnimal;
            int animalCount = 0;
            int sumPoints = 0;

            //Read data and evaluate contestant one-by-one
            do
            {
                //Read contestant data
                animal = GetAnimalFromConsole();

                //Make evaluation of selected type (based on previous selection)
                if (randomEvaluation)
                {
                    EvaluateRandomly(animal);
                }
                else
                {
                    EvaluateFromConsole(animal);
                }

                //Write evaluation result to console
                Console.WriteLine(animal);

                animalCount++;
                sumPoints += animal.TotalPoints;

                //Store the winner (highest total points)
                if (winner == null || winner.TotalPoints < animal.TotalPoints)
                {
                    winner = animal;
                }

                //Check if there is another contestant
                hasAnotherAnimal = ConsoleInput.GetKeyFromConsole("Is there another contestant? (y/n) ", 'y');
            } while (hasAnotherAnimal);

            //Print the winner
            Console.WriteLine("The winner is {0}", winner);
            Console.WriteLine("Number of animals: {0}", animalCount);
            Console.WriteLine("Sum of points: {0}", sumPoints);
            Console.WriteLine("Average of points: {0}", sumPoints / animalCount);
        }

        private Animal GetAnimalFromConsole()
        {
            Console.Write("Name of contestant: ");
            string name = ConsoleInput.GetStringFromConsole();
            Console.Write("Birth year: ");
            int birthYear = ConsoleInput.GetIntFromConsole(BIRTH_MIN, BIRTH_MAX);
            Console.Write("Vaccination Certification number: ");
            string vaccCerNum = ConsoleInput.GetStringFromConsole();

            currentId += 1;

            return new Animal(name, birthYear, vaccCerNum, currentId);
        }

        private void EvaluateFromConsole(Animal animal)
        {
            Console.WriteLine("Evaluation of {0}", animal.Name);
            Console.Write("Beauty: ");
            int beauty = ConsoleInput.GetIntFromConsole(pointMin, pointMax);
            Console.Write("Behaviour: ");
            int behaviour = ConsoleInput.GetIntFromConsole(pointMin, pointMax);

            animal.Evaluation(beauty, behaviour);
        }

        private void EvaluateRandomly(Animal animal)
        {
            animal.Evaluation(
                random.Next(pointMin, pointMax + 1),
                random.Next(pointMin, pointMax + 1)
            );
        }
    }
}
