﻿using System;

namespace AnimalContest
{
    //A class to encapsulate console input functions
    class ConsoleInput
    {
        public static int GetIntFromConsole(int min, int max)
        {
            int number;
            while (!int.TryParse(Console.ReadLine(), out number) || number < min || number > max)
            {
                Console.WriteLine("Please enter an integer ({0}-{1})!", min, max);
            }
            return number;
        }

        public static bool GetKeyFromConsole(string message, char lookFor)
        {
            Console.Write(message);
            ConsoleKeyInfo key = Console.ReadKey();
            Console.WriteLine();
            return char.ToLower(key.KeyChar) == char.ToLower(lookFor);
        }

        public static string GetStringFromConsole()
        {
            return Console.ReadLine();
        }
    }
}
