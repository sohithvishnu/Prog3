﻿namespace Workers
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.input = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.workerList = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.checkSalary = new System.Windows.Forms.Button();
            this.checkResult = new System.Windows.Forms.ListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.name = new System.Windows.Forms.TextBox();
            this.job = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.salary = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.save = new System.Windows.Forms.Button();
            this.process = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.salary)).BeginInit();
            this.SuspendLayout();
            // 
            // input
            // 
            this.input.Location = new System.Drawing.Point(15, 29);
            this.input.Multiline = true;
            this.input.Name = "input";
            this.input.Size = new System.Drawing.Size(764, 265);
            this.input.TabIndex = 0;
            this.input.TextChanged += new System.EventHandler(this.input_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Text input:";
            // 
            // workerList
            // 
            this.workerList.Enabled = false;
            this.workerList.FormattingEnabled = true;
            this.workerList.ItemHeight = 16;
            this.workerList.Location = new System.Drawing.Point(15, 317);
            this.workerList.Name = "workerList";
            this.workerList.Size = new System.Drawing.Size(367, 164);
            this.workerList.TabIndex = 2;
            this.workerList.SelectedIndexChanged += new System.EventHandler(this.workerList_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 297);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Workers:";
            // 
            // checkSalary
            // 
            this.checkSalary.Enabled = false;
            this.checkSalary.Location = new System.Drawing.Point(388, 386);
            this.checkSalary.Name = "checkSalary";
            this.checkSalary.Size = new System.Drawing.Size(122, 26);
            this.checkSalary.TabIndex = 4;
            this.checkSalary.Text = "Check";
            this.checkSalary.UseVisualStyleBackColor = true;
            this.checkSalary.Click += new System.EventHandler(this.checkSalary_Click);
            // 
            // checkResult
            // 
            this.checkResult.FormattingEnabled = true;
            this.checkResult.ItemHeight = 16;
            this.checkResult.Location = new System.Drawing.Point(516, 317);
            this.checkResult.Name = "checkResult";
            this.checkResult.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.checkResult.Size = new System.Drawing.Size(367, 164);
            this.checkResult.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 490);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 17);
            this.label3.TabIndex = 6;
            this.label3.Text = "Name:";
            // 
            // name
            // 
            this.name.Location = new System.Drawing.Point(70, 487);
            this.name.Name = "name";
            this.name.ReadOnly = true;
            this.name.Size = new System.Drawing.Size(312, 22);
            this.name.TabIndex = 7;
            // 
            // job
            // 
            this.job.Location = new System.Drawing.Point(70, 515);
            this.job.Name = "job";
            this.job.ReadOnly = true;
            this.job.Size = new System.Drawing.Size(312, 22);
            this.job.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 518);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Job:";
            // 
            // salary
            // 
            this.salary.Enabled = false;
            this.salary.Location = new System.Drawing.Point(70, 546);
            this.salary.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.salary.Name = "salary";
            this.salary.Size = new System.Drawing.Size(124, 22);
            this.salary.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 549);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 17);
            this.label5.TabIndex = 11;
            this.label5.Text = "Salary:";
            // 
            // save
            // 
            this.save.Enabled = false;
            this.save.Location = new System.Drawing.Point(310, 543);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(72, 28);
            this.save.TabIndex = 12;
            this.save.Text = "Save";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // process
            // 
            this.process.Location = new System.Drawing.Point(785, 29);
            this.process.Name = "process";
            this.process.Size = new System.Drawing.Size(97, 264);
            this.process.TabIndex = 13;
            this.process.Text = "Process";
            this.process.UseVisualStyleBackColor = true;
            this.process.Click += new System.EventHandler(this.process_Click);
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(895, 606);
            this.Controls.Add(this.process);
            this.Controls.Add(this.save);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.salary);
            this.Controls.Add(this.job);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.name);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.checkResult);
            this.Controls.Add(this.checkSalary);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.workerList);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.input);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "mainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Worker list";
            this.Load += new System.EventHandler(this.mainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.salary)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox input;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox workerList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button checkSalary;
        private System.Windows.Forms.ListBox checkResult;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox name;
        private System.Windows.Forms.TextBox job;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown salary;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button save;
        private System.Windows.Forms.Button process;
    }
}

