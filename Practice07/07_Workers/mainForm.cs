﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Workers
{
    public partial class mainForm : Form
    {
        private WorkerCollection workers;

        public mainForm()
        {
            workers = new WorkerCollection();

            InitializeComponent();
        }

        //Form load event handler (runs after loading the form)
        private void mainForm_Load(object sender, EventArgs e)
        {
            try
            {
                LoadInputString();
            } 
            catch(Exception ex)
            {
                MessageBox.Show(
                    string.Format("An error occurred!{0}{1}", Environment.NewLine, ex.Message),
                    "Error",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                );
            }

        }

        //Process button click event handler
        private void process_Click(object sender, EventArgs e)
        {
            workers.Clear();
            ProcessInput();
            ListWorkersInListBox();
            SetListActionsAccessibility();
            SetItemActionsAccessibility();
        }

        //Event handler of text changed in input TextBox
        private void input_TextChanged(object sender, EventArgs e)
        {
            process.Enabled = input.Text.Length > 0;
        }

        //Event handler of changing selected item index in workerList
        private void workerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetItemActionsAccessibility();

            Worker selectedWorker = workerList.SelectedItem as Worker;

            if(selectedWorker != null)
            {
                name.Text = selectedWorker.Name;
                job.Text = selectedWorker.Job;
                salary.Value = selectedWorker.Salary;
            }
            else
            {
                name.Text = "";
                job.Text = "";
                salary.Value = 0;
            }
        }

        //Save button click event handler
        private void save_Click(object sender, EventArgs e)
        {
            Worker selectedWorker = workerList.SelectedItem as Worker;

            if (selectedWorker != null)
            {
                selectedWorker.Salary = (int)salary.Value;
            }
        }

        //checkSalary button click event handler
        private void checkSalary_Click(object sender, EventArgs e)
        {
            checkResult.Items.Clear();
            foreach(Worker w in workers.CheckSalaryAbilities(w => w.Salary > 3000 && w.Abilities.Count < 2))
            {
                checkResult.Items.Add(
                    string.Format("{0} {1}:{2}",
                        w.Name, w.Salary, w.Abilities.Count
                    )
                );
            }
        }

        #region Private methods

        private void LoadInputString()
        {
            try
            {
                StreamReader sr = new StreamReader("..\\..\\Data\\Workers.txt");
                input.Text = sr.ReadToEnd();
                sr.Close();
            }
            catch(Exception ex)
            {
                input.Text = String.Format("Could not load file: {0}", ex.Message);
            }
        }

        private void ProcessInput()
        {
            try
            {
                //Load whole list of workers
                //using a loader injected into the storage
                workers.LoadFromMultilineText(
                    new WorkerTextParser(),
                    input.Text
                );
            }
            catch(Exception ex)
            {
                MessageBox.Show(
                    string.Format("Data processing exception occurred!{0}{1}",
                        Environment.NewLine, ex.Message
                    ),
                    "Attention"
                );
            }
        }

        private void SetListActionsAccessibility()
        {
            workerList.Enabled = workers.Count > 0;
            checkSalary.Enabled = workerList.Enabled;
        }

        private void SetItemActionsAccessibility()
        {
            salary.Enabled = workerList.SelectedItem != null;
            save.Enabled = salary.Enabled;
        }

        private void ListWorkersInListBox()
        {
            workerList.Items.Clear();

            foreach(Worker w in workers)
            {
                workerList.Items.Add(w);
            }
        }

        #endregion
    }
}
