﻿using System.Collections.Generic;

namespace Workers
{
    class Worker
    {
        //Name formatter string 0-FirstName, 1-LastName
        public static string NameFormat { get; set; } = "{1} {0}";

        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public string Job { get; private set; }
        public int Salary { get; set; }

        //Abilities list is published via getter, the list reference can not be changed
        //but the contents of the list can. And this is not a problem.
        //We do want to provide an option for the user to change Abilities of a worker
        public List<Ability> Abilities { get; private set; }

        public string Name
        {
            get
            {
                return string.Format(NameFormat, FirstName, LastName);
            }
        }

        public Worker(string firstName, string lastName, string job, int salary, string abilityString)
        {
            FirstName = firstName;
            LastName = lastName;
            Job = job;
            Salary = salary;

            Abilities = new List<Ability>();
            SetAbilities(abilityString);
        }

        //Set abilities from a string (of dictionary - key:value pairs)
        private void SetAbilities(string abilityString)
        {
            //Remove { prefix and } suffix from string
            //Split to ability key:value pairs
            string[] abilities = abilityString
                .Substring(1, abilityString.Length - 2)
                .Trim()
                .Split(';');

            Abilities.Clear();
            //Parse individual abilities
            foreach (string ab in abilities)
            {
                string[] abilityProps = ab.Trim().Split(':');
                if (abilityProps.Length == 2)
                {
                    Abilities.Add(
                        new Ability(
                            abilityProps[0].Trim(),
                            int.Parse(abilityProps[1].Trim())
                            )
                    );
                }
            }
        }

        public override string ToString()
        {
            return string.Format(
                "{0} ({1}) [{2}]", 
                Name, Job, Abilities.Count
            );
        }
    }
}
