﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Workers
{
    class WorkerCollection : IEnumerable
    {
        private List<Worker> workers;

        public WorkerCollection()
        {
            workers = new List<Worker>();
        }

        //Collection can initialize itself from a multiline text
        //using an injected parsed (therefore collection is not responsible for text structure)
        public void LoadFromMultilineText(ITextParser<Worker> parser, string multilineText)
        {
            workers.Clear();
            workers.AddRange(
                parser.ParseListFromMultilineText(multilineText)
            );
        }

        #region Components to provide list functionality

        public void Clear()
        {
            workers.Clear();
        }

        public void Add(Worker newWorker)
        {
            workers.Add(newWorker);
        }

        public Worker this[int i]
        {
            get { return workers[i]; }
        }

        public int Count
        {
            get { return workers.Count; }
        }

        #endregion

        #region IEnumerable interface implementation

        public IEnumerator GetEnumerator()
        {
            return new WorkerCollectionEnumerator(this);
        }

        #endregion

        //Filter overpaid workers
        public List<Worker> CheckSalaryAbilities(Func<Worker, bool> workerSelector)
        {
            List<Worker> results = new List<Worker>();
            foreach (Worker w in workers)
            {
                if (workerSelector(w))
                {
                    results.Add(w);
                }
            }
            return results;
        }
    }
}
