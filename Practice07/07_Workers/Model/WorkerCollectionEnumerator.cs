﻿using System.Collections;

namespace Workers
{
    class WorkerCollectionEnumerator : IEnumerator
    {
        WorkerCollection collection;
        int index = -1;

        //Overloaded specific Current property
        public Worker Current => collection[index];

        //Current property declared in IEnumerator interface
        object IEnumerator.Current => Current;

        public WorkerCollectionEnumerator(WorkerCollection collection)
        {
            this.collection = collection;
        }

        public bool MoveNext()
        {
            index++;
            return index < collection.Count;
        }

        public void Reset()
        {
            index = -1;
        }
    }
}
