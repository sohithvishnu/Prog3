﻿namespace Kozepek
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Compute = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.X = new System.Windows.Forms.TextBox();
            this.Y = new System.Windows.Forms.TextBox();
            this.Avg1 = new System.Windows.Forms.TextBox();
            this.Avg2 = new System.Windows.Forms.TextBox();
            this.Avg3 = new System.Windows.Forms.TextBox();
            this.Clear = new System.Windows.Forms.Button();
            this.Finish = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Compute
            // 
            this.Compute.Location = new System.Drawing.Point(27, 204);
            this.Compute.Name = "Compute";
            this.Compute.Size = new System.Drawing.Size(75, 33);
            this.Compute.TabIndex = 0;
            this.Compute.Text = "Számol";
            this.Compute.UseVisualStyleBackColor = true;
            this.Compute.Click += new System.EventHandler(this.Compute_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(40, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "X";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(146, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Y";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 92);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(66, 17);
            this.label3.TabIndex = 3;
            this.label3.Text = "Számtani";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 120);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mértani";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Harmonikus";
            // 
            // X
            // 
            this.X.Location = new System.Drawing.Point(43, 33);
            this.X.Name = "X";
            this.X.Size = new System.Drawing.Size(100, 22);
            this.X.TabIndex = 6;
            // 
            // Y
            // 
            this.Y.Location = new System.Drawing.Point(149, 33);
            this.Y.Name = "Y";
            this.Y.Size = new System.Drawing.Size(100, 22);
            this.Y.TabIndex = 7;
            // 
            // Avg1
            // 
            this.Avg1.Location = new System.Drawing.Point(102, 89);
            this.Avg1.Name = "Avg1";
            this.Avg1.ReadOnly = true;
            this.Avg1.Size = new System.Drawing.Size(100, 22);
            this.Avg1.TabIndex = 8;
            // 
            // Avg2
            // 
            this.Avg2.Location = new System.Drawing.Point(102, 117);
            this.Avg2.Name = "Avg2";
            this.Avg2.ReadOnly = true;
            this.Avg2.Size = new System.Drawing.Size(100, 22);
            this.Avg2.TabIndex = 9;
            // 
            // Avg3
            // 
            this.Avg3.Location = new System.Drawing.Point(102, 145);
            this.Avg3.Name = "Avg3";
            this.Avg3.ReadOnly = true;
            this.Avg3.Size = new System.Drawing.Size(100, 22);
            this.Avg3.TabIndex = 10;
            // 
            // Clear
            // 
            this.Clear.Location = new System.Drawing.Point(102, 204);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(75, 33);
            this.Clear.TabIndex = 11;
            this.Clear.Text = "Törlés";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // Finish
            // 
            this.Finish.Location = new System.Drawing.Point(177, 204);
            this.Finish.Name = "Finish";
            this.Finish.Size = new System.Drawing.Size(75, 33);
            this.Finish.TabIndex = 12;
            this.Finish.Text = "Vége";
            this.Finish.UseVisualStyleBackColor = true;
            this.Finish.Click += new System.EventHandler(this.Finish_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 249);
            this.Controls.Add(this.Finish);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.Avg3);
            this.Controls.Add(this.Avg2);
            this.Controls.Add(this.Avg1);
            this.Controls.Add(this.Y);
            this.Controls.Add(this.X);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Compute);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Közepek";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Compute;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox X;
        private System.Windows.Forms.TextBox Y;
        private System.Windows.Forms.TextBox Avg1;
        private System.Windows.Forms.TextBox Avg2;
        private System.Windows.Forms.TextBox Avg3;
        private System.Windows.Forms.Button Clear;
        private System.Windows.Forms.Button Finish;
    }
}

