﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GumiSzerviz
{
    class TeliGumi : Gumi
    {
        public static int EngedelyezettTelekSzama { get; set; }

        public int TelekSzama { get; private set; }

        public TeliGumi(String kod, int telekSzama) : base(kod)
        {
            TelekSzama = telekSzama;
        }

        public override bool Megfelel()
        {
            return base.Megfelel() && TelekSzama <= EngedelyezettTelekSzama;
        }

        public override string ToString()
        {
            return String.Format("{0}, {1} telet futott", 
                base.ToString(), TelekSzama
            );
        }
    }
}
