﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GumiSzerviz
{
    class Szerviz
    {
        const int GUMIK_SZAMA = 4;

        int aktualisHomerseklet;
        Random random = new Random();

        List<Gumi> vizsgaltGumik = new List<Gumi>();
        List<Gumi> leszereltGumik = new List<Gumi>();

        private void Inicializalas()
        {
            Gumi.GyartasiProfilMelyseg = 20;
            Gumi.MinAllapot = 20;
            Gumi.ValtoHomerseklet = 7;

            TeliGumi.EngedelyezettTelekSzama = 5;
        }

        public void NapKezdes()
        {
            Inicializalas();

            aktualisHomerseklet = EgeszBekeres("Aktuális hőmérséklet");
        }

        private int EgeszBekeres(String uzenet)
        {
            Console.Write(String.Format("{0}: ", uzenet));
            int egesz;
            while (!int.TryParse(Console.ReadLine(), out egesz))
            {
                Console.Write(String.Format("{0} (egész szám): ", uzenet));
            }

            return egesz;
        }

        public void MunkaFelvetel()
        {
            StreamReader reader = new StreamReader("autok.txt");
            String rendszam;
            String gumiTipus;
            String gumiAzonosító;
            Gumi vizsgaltGumi;
            while (!reader.EndOfStream)
            {
                rendszam = reader.ReadLine();
                gumiTipus = reader.ReadLine();

                Console.WriteLine("A {0} rendszámú jármű {1} gumijai:",
                    rendszam, gumiTipus
                );

                for (int i = 0; i < GUMIK_SZAMA; i++)
                {
                    gumiAzonosító = reader.ReadLine();

                    Console.WriteLine("A {0} azonosítójú gumi vizsgálata...",
                        gumiAzonosító
                    );

                    if (gumiTipus == "téli")
                    {
                        vizsgaltGumi = new TeliGumi(
                            gumiAzonosító,
                            EgeszBekeres("Futott telek száma")
                            );
                    }
                    else
                    {
                        vizsgaltGumi = new Gumi(
                            gumiAzonosító
                            );
                    }

                    vizsgaltGumik.Add(vizsgaltGumi);
                    Vizsgalat(vizsgaltGumi);
                }
            }
            reader.Close();
        }

        private void Vizsgalat(Gumi gumi)
        {
            gumi.ProfilMelyseg = random.Next(Gumi.GyartasiProfilMelyseg);
            Console.WriteLine("Mért profilmélység: {0}", gumi.ProfilMelyseg);

            if (!gumi.Megfelel() || HomersekletAlapuCsere(gumi))
            {
                Console.WriteLine("A {0} nem megfelelő.\nSzeretné cserélni? (i/n)", gumi);
                ConsoleKeyInfo key;
                do
                {
                    key = Console.ReadKey();
                } while (
                    Char.ToLower(key.KeyChar) != 'i' 
                    && Char.ToLower(key.KeyChar) != 'n'
                );

                Console.WriteLine();

                if (Char.ToLower(key.KeyChar) == 'i')
                {
                    leszereltGumik.Add(gumi);
                    Console.WriteLine("Gumi LESZERELVE");
                }
            }
        }

        private bool HomersekletAlapuCsere(Gumi gumi)
        {
            return
                (gumi is TeliGumi && aktualisHomerseklet >= Gumi.ValtoHomerseklet)
                ||
                (!(gumi is TeliGumi) && aktualisHomerseklet < Gumi.ValtoHomerseklet);
        }

        public void LeszereltTeliGumik()
        {
            foreach(Gumi gumi in leszereltGumik)
            {
                if(gumi is TeliGumi)
                {
                    Console.WriteLine(gumi);
                }
            }
        }

        public int AtlagosAllapot()
        {
            int osszAllapot = 0;
            foreach (Gumi gumi in vizsgaltGumik)
            {
                osszAllapot += gumi.Allapot;
            }
            return osszAllapot / vizsgaltGumik.Count;
        }

        public void LegjobbGumik()
        {
            int maxAllapot = 0;
            foreach (Gumi gumi in vizsgaltGumik)
            {
                if (maxAllapot < gumi.Allapot)
                    maxAllapot = gumi.Allapot;
            }
            foreach (Gumi gumi in vizsgaltGumik)
            {
                if (maxAllapot == gumi.Allapot)
                    Console.WriteLine(gumi);
            }
        }
    }
}
