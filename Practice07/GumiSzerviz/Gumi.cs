﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GumiSzerviz
{
    class Gumi
    {
        public static int GyartasiProfilMelyseg { get; set; }
        public static int MinAllapot { get; set; }
        public static int ValtoHomerseklet { get; set; }

        public String Kod { get; private set; }
        public int ProfilMelyseg { get; set; }

        public int Allapot
        {
            get
            {
                return (ProfilMelyseg * 100) / GyartasiProfilMelyseg;
            }
        }

        public Gumi(String kod)
        {
            Kod = kod;
        }

        public virtual bool Megfelel()
        {
            return Allapot > MinAllapot;
        }

        public override string ToString()
        {
            return String.Format("{0} azonotítójú {1}, állapota {2}%", 
                Kod, this.GetType().Name, Allapot
            );
        }
    }
}
