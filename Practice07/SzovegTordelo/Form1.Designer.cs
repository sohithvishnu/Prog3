﻿namespace SzovegTordelo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.Sentence = new System.Windows.Forms.TextBox();
            this.Brake = new System.Windows.Forms.Button();
            this.Words = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Kérem a szöveget";
            // 
            // Sentence
            // 
            this.Sentence.Location = new System.Drawing.Point(15, 29);
            this.Sentence.Name = "Sentence";
            this.Sentence.Size = new System.Drawing.Size(773, 22);
            this.Sentence.TabIndex = 1;
            this.Sentence.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Sentence_KeyDown);
            // 
            // Brake
            // 
            this.Brake.Location = new System.Drawing.Point(15, 225);
            this.Brake.Name = "Brake";
            this.Brake.Size = new System.Drawing.Size(119, 42);
            this.Brake.TabIndex = 3;
            this.Brake.Text = "Szövegtördelés";
            this.Brake.UseVisualStyleBackColor = true;
            this.Brake.Click += new System.EventHandler(this.Brake_Click);
            // 
            // Words
            // 
            this.Words.Location = new System.Drawing.Point(140, 57);
            this.Words.Name = "Words";
            this.Words.Size = new System.Drawing.Size(648, 381);
            this.Words.TabIndex = 4;
            this.Words.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Words);
            this.Controls.Add(this.Brake);
            this.Controls.Add(this.Sentence);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Szöveg tördelés";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Sentence;
        private System.Windows.Forms.Button Brake;
        private System.Windows.Forms.RichTextBox Words;
    }
}

