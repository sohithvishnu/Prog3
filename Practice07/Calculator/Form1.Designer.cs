﻿namespace Calculator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Op1 = new System.Windows.Forms.TextBox();
            this.Op2 = new System.Windows.Forms.TextBox();
            this.Result = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Add = new System.Windows.Forms.Button();
            this.Sub = new System.Windows.Forms.Button();
            this.Mul = new System.Windows.Forms.Button();
            this.DivInt = new System.Windows.Forms.Button();
            this.DivRem = new System.Windows.Forms.Button();
            this.Clear = new System.Windows.Forms.Button();
            this.Finish = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(47, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Op 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(252, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Op 2";
            // 
            // Op1
            // 
            this.Op1.Location = new System.Drawing.Point(50, 29);
            this.Op1.Name = "Op1";
            this.Op1.Size = new System.Drawing.Size(100, 22);
            this.Op1.TabIndex = 2;
            // 
            // Op2
            // 
            this.Op2.Location = new System.Drawing.Point(255, 29);
            this.Op2.Name = "Op2";
            this.Op2.Size = new System.Drawing.Size(100, 22);
            this.Op2.TabIndex = 3;
            // 
            // Result
            // 
            this.Result.Location = new System.Drawing.Point(146, 208);
            this.Result.Name = "Result";
            this.Result.ReadOnly = true;
            this.Result.Size = new System.Drawing.Size(100, 22);
            this.Result.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(68, 211);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 17);
            this.label3.TabIndex = 5;
            this.label3.Text = "Eredmény";
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(157, 63);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(75, 23);
            this.Add.TabIndex = 6;
            this.Add.Text = "+";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // Sub
            // 
            this.Sub.Location = new System.Drawing.Point(157, 92);
            this.Sub.Name = "Sub";
            this.Sub.Size = new System.Drawing.Size(75, 23);
            this.Sub.TabIndex = 7;
            this.Sub.Text = "-";
            this.Sub.UseVisualStyleBackColor = true;
            this.Sub.Click += new System.EventHandler(this.Sub_Click);
            // 
            // Mul
            // 
            this.Mul.Location = new System.Drawing.Point(157, 121);
            this.Mul.Name = "Mul";
            this.Mul.Size = new System.Drawing.Size(75, 23);
            this.Mul.TabIndex = 8;
            this.Mul.Text = "*";
            this.Mul.UseVisualStyleBackColor = true;
            this.Mul.Click += new System.EventHandler(this.Mul_Click);
            // 
            // DivInt
            // 
            this.DivInt.Location = new System.Drawing.Point(157, 150);
            this.DivInt.Name = "DivInt";
            this.DivInt.Size = new System.Drawing.Size(75, 23);
            this.DivInt.TabIndex = 9;
            this.DivInt.Text = "/";
            this.DivInt.UseVisualStyleBackColor = true;
            this.DivInt.Click += new System.EventHandler(this.DivInt_Click);
            // 
            // DivRem
            // 
            this.DivRem.Location = new System.Drawing.Point(157, 179);
            this.DivRem.Name = "DivRem";
            this.DivRem.Size = new System.Drawing.Size(75, 23);
            this.DivRem.TabIndex = 10;
            this.DivRem.Text = "%";
            this.DivRem.UseVisualStyleBackColor = true;
            this.DivRem.Click += new System.EventHandler(this.DivRem_Click);
            // 
            // Clear
            // 
            this.Clear.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Clear.Location = new System.Drawing.Point(50, 244);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(75, 37);
            this.Clear.TabIndex = 11;
            this.Clear.Text = "Töröl";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // Finish
            // 
            this.Finish.Location = new System.Drawing.Point(280, 244);
            this.Finish.Name = "Finish";
            this.Finish.Size = new System.Drawing.Size(75, 37);
            this.Finish.TabIndex = 12;
            this.Finish.Text = "Vege";
            this.Finish.UseVisualStyleBackColor = true;
            this.Finish.Click += new System.EventHandler(this.Finish_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(383, 293);
            this.Controls.Add(this.Finish);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.DivRem);
            this.Controls.Add(this.DivInt);
            this.Controls.Add(this.Mul);
            this.Controls.Add(this.Sub);
            this.Controls.Add(this.Add);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.Result);
            this.Controls.Add(this.Op2);
            this.Controls.Add(this.Op1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox Op1;
        private System.Windows.Forms.TextBox Op2;
        private System.Windows.Forms.TextBox Result;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Sub;
        private System.Windows.Forms.Button Mul;
        private System.Windows.Forms.Button DivInt;
        private System.Windows.Forms.Button DivRem;
        private System.Windows.Forms.Button Clear;
        private System.Windows.Forms.Button Finish;
    }
}

