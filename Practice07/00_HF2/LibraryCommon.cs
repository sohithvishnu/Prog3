﻿using System;
using System.Collections.Generic;

namespace _00_HF2
{
    class LibraryCommon : Library
    {
        //Creates (and returns) a list of items which satisty filter criterion
        //(wher filter returns true for the item)
        //With this implementation, creation of explicitly typed list is lost
        public List<Publication> Find(Func<Publication, bool> filter)
        {
            List<Publication> found = new List<Publication>();

            foreach (var p in publications)
            {
                if (filter(p))
                {
                    found.Add(p);
                }
            }

            return found;
        }

        //Uses Find method with a filter which returns all elements
        //(returns true for all elements)
        //that will create a copy of the original list, 
        //instead of publishing the class state data member
        public List<Publication> GetAll()
        {
            return Find((p) => true);
        }
    }
}
