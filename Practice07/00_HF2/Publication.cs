﻿namespace _00_HF2
{
    abstract class Publication
    {
        //Class property (automatic)
        public static int BasePrice { get; set; }

        //Instance properties (automatic)
        public string Title { get; private set; }
        public string Publisher { get; private set; }

        //Applicatin of instance property makes Price overridable, 
        //but utilizes class data
        public virtual int Price { get { return BasePrice; } }

        public Publication(string title, string publisher)
        {
            Title = title;
            Publisher = publisher;
        }

        //Method is not abstract, because rental price can be calculated
        //but virtual to provide an option to specialize rental price calculation
        public virtual int RentalPrice(int days)
        {
            return Price * days;
        }
    }
}
