﻿namespace _00_HF2
{
    class Book : Publication
    {
        //Specialized class property (automatic)
        public static int AdditionalPrice { get; set; }

        //Specialized instance properties (automatic)
        public string Author { get; private set; }
        public int PageNumber { get; private set; }

        //Specialized computed instance property to utilize new price component
        public override int Price { get { return base.Price + AdditionalPrice; } }

        public Book(string title, string publisher,
            string author, int pageNumber
            ) : base(title, publisher)
        {
            Author = author;
            PageNumber = pageNumber;
        }

        //Specialized price calculation
        //uses price calculation of base class
        //Overriding of this method is not required, because base implementation
        //uses overrode version of instance property, therefore base Price behaviour has been changed
        public override int RentalPrice(int days)
        {
            return base.RentalPrice(days) + AdditionalPrice * days;
        }

        public override string ToString()
        {
            return string.Format("{0}: {1} ({2}) [{3}/day]",
                Author, Title, Publisher, Price);
        }
    }
}
