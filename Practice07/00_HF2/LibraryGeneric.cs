﻿using System;
using System.Collections.Generic;

namespace _00_HF2
{
    class LibraryGeneric : LibraryCommon
    {
        //Creates (and returns) a list of items of a specific type
        //which satisty filter criterion (wher filter returns true for the item)
        //With application and restriction of type parameter, explicitly typed list can be returned
        public List<T> Find<T>(Func<Publication, bool> filter) where T : Publication
        {
            List<T> found = new List<T>();

            foreach (var p in publications)
            {
                if (p is T && filter(p))
                {
                    found.Add((T)p);
                }
            }

            return found;
        }
    }
}
